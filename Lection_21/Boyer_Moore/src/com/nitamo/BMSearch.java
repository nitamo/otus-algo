package com.nitamo;

public class BMSearch {
    private static int alphabetIndex(char c) {
        c = Character.toLowerCase(c);
        return c - 97;
    }

    private static int[] getBadCharArray(String pattern) {
        var table = new int[26];

        for(int i = 0; i < 26; i++) {
            table[i] = pattern.length();
        }

        for (int i = 0; i < pattern.length(); i++) {
            var index = alphabetIndex(pattern.charAt(i));
            table[index] = pattern.length() - i - 1;
        }

        return table;
    }

    private static int getSuffixLength(String pattern, int position) {
        var len = pattern.length();
        int i = 0;
        while((pattern.charAt(position - i) == pattern.charAt(len - 1 - i) && (i < position))) {
            i++;
        }
        return i;
    }

    private static boolean isPrefix(String pattern, int position) {
        for (int i = 0; i < pattern.length() - position; i++) {
            if(pattern.charAt(i) != pattern.charAt(position + i)) {
                return false;
            }
        }
        return true;
    }

    private static int[] getSuffixArray(String pattern) {
        var length = pattern.length();
        var result = new int[length];
        int last_prefix_index = length - 1;

        for(int p = length - 1; p >= 0; p--) {
            if(isPrefix(pattern, p + 1)) {
                last_prefix_index = p + 1;
            }
            result[p] = last_prefix_index + (length - 1 - p);
        }

        for (int p = 0; p < length-1; p++) {
            var suffixLength = getSuffixLength(pattern, p);
            if (pattern.charAt(p - suffixLength) != pattern.charAt(length-1 - suffixLength)) {
                result[length-1 - suffixLength] = length-1 - p + suffixLength;
            }
        }
        return result;
    }

    static int search(String text, String pattern) {

        var badCharArray = getBadCharArray(pattern);
        var suffixArray = getSuffixArray(pattern);

        if (pattern.length() == 0) {
            return 0;
        }

        for (int i = pattern.length() - 1; i < text.length();) {
            int j = pattern.length() - 1;
            for (; j >= 0 && (text.charAt(i) == pattern.charAt(j)); i--, j--) {
            }
            if(j < 0) {
                return i + 1;
            }
            i += Math.max(badCharArray[alphabetIndex(text.charAt(i))], suffixArray[j]);
        }
        return 0;
    }

    private static int[] findPrefixes(String pattern) {
        var result = new int[pattern.length()];

        for (int i = 0; i < pattern.length(); i++) {
            for (int j = 0; i + j < pattern.length(); j++) {
                if(pattern.charAt(i + j) == pattern.charAt(j)) {
                    result[i]++;
                }
            }
        }

        return result;
    }
}
