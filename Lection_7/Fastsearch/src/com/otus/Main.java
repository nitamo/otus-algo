package com.otus;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;

public class Main {

    public static void main(String[] args) {
        //long[] array = new long[] {38, 27, 43, 3, 9, 28, 10};
        //long[] array = new long[] {3, 2, 1, 0, 5, 4};
        long[] array = (new Random()).longs(100_000_000, 0, 100_000_000).toArray();

        var startTime = System.currentTimeMillis();
        //System.out.println("Source: " + Arrays.toString(array));
        MergeSort.MergeSort(array);
        //System.out.println("Sorted: " + Arrays.toString(array));
        System.out.println("Time elapsed: " + (System.currentTimeMillis() - startTime));
    }
}

class MergeSort {
    private static ForkJoinPool forkJoinPool = new ForkJoinPool();

    public static void MergeSort(long[] array) {
        long[] copy = new long[array.length];
        System.arraycopy(array, 0, copy, 0, copy.length);
        MergeSort.SplitMerge(array, copy, 0, array.length);
    }

    public static void SplitMerge(long[] array, long[] copy, int begin, int end) {
        if(end - begin < 10) {
            InsertionSort(array, begin, end);
            return;
        }

        int middle = (end + begin) / 2;
        var left = forkJoinPool.submit(() -> SplitMerge(copy, array, begin, middle));
        var right = forkJoinPool.submit(() -> SplitMerge(copy, array, middle, end));
        left.join();
        right.join();
        Merge(copy, array, begin, middle, end);
    }

    private static void Merge(long[] array, long[] copy, int begin, int middle, int end) {
        int first = begin;
        int second = middle;
        for(int i = begin; i < end; i++) {
            if(first < middle && (second >= end || array[first] <= array[second])) {
                copy[i] = array[first];
                first++;
            }
            else {
                copy[i] = array[second];
                second++;
            }
        }
    }

    public static void InsertionSort(long[] array, int begin, int end) {
        for(int i = begin + 1; i < end; i++) {
            long temp = array[i];
            for(int j = i; j > begin && array[j - 1] > temp; j--) {
                array[j] = array[j - 1];
                array[j - 1] = temp;
            }
        }
    }
}
