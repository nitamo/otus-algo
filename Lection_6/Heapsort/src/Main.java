import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        //long[] array = new long[] {9,5,10,8,2,1,0,3,11,4,6,7};
        long[] array = (new Random()).longs(12, 0, 12).toArray();
        System.out.println(Arrays.toString(array));
        Heap.build(array);
        System.out.println(Arrays.toString(array));
        Heap.sort(array);
        System.out.println(Arrays.toString(array));
    }
}

class Heap {
    static void build(long[] array) {
        for(int i = (array.length - 1) / 2; i >= 0; i-- ) {
            drownIter(array, i, array.length);
        }
    }

    static void sort(long[] array) {
        int size = array.length;
        for(int i = array.length - 1; i > 0; i--) {
            swap(array, i, 0);
            size--;
            drownIter(array, 0, size);
        }
    }

    static long[] removeMax(long[] array) {
        long[] result = new long[array.length - 1];
        result[0] = array[array.length - 1];
        System.arraycopy(array, 1, result, 1, result.length - 1);
        drownIter(result, 0, result.length);
        return result;
    }

    private static void drown(long[] array, int i, int size) {
        int left = i * 2 + 1;
        int right = i * 2 + 2;
        int largest;

        if(left < size && array[left] > array[i]) {
            largest = left;
        }
        else {
            largest = i;
        }

        if(right < size && array[right] > array[largest]) {
            largest = right;
        }

        if(largest != i) {
            swap(array, i, largest);
            drown(array, largest, size);
        }
    }

    private static void drownIter(long[] array, int i, int size) {

        for(int largest; true; i = largest) {
            int left = i * 2 + 1;
            int right = i * 2 + 2;

            if(left < size && array[left] > array[i]) {
                largest = left;
            }
            else {
                largest = i;
            }

            if(right < size && array[right] > array[largest]) {
                largest = right;
            }

            if(largest != i) {
                swap(array, i, largest);
            }
            else {
                break;
            }
        }
    }

    private static void swap(long[] array, int i, int largest) {
        long temp = array[i];
        array[i] = array[largest];
        array[largest] = temp;
    }
}