package com.nitamo;

import java.util.ArrayList;

public class KMP {
    private static int[] computePrefix(String pattern) {
        var table = new int[pattern.length()];
        int k = 0;
        for (int i = 1; i < pattern.length(); i++) {
            while (k > 0 && (pattern.charAt(k) != pattern.charAt(i))) {
                k = table[k-1];
            }
            if(pattern.charAt(k) == pattern.charAt(i)) {
                k++;
            }
            table[i] = k;
        }
        return table;
    }

    public static int[] search(String text, String pattern) {
        var matches = new ArrayList<Integer>();
        var table = computePrefix(pattern);

        int q = 0;
        for (int i = 0; i < text.length(); i++) {
            while (q > 0 && pattern.charAt(q) != text.charAt(i)) {
                q = table[q - 1];
            }
            if(pattern.charAt(q) == text.charAt(i)) {
                q++;
            }
            if(q == pattern.length()) {
                matches.add(i - pattern.length() + 1);
                q = table[q - 1];
            }
        }

        //Return result
        var result = new int[matches.size()];
        for (int i = 0; i < matches.size(); i++) {
            result[i] = matches.get(i);
        }
        return result;
    }
}
