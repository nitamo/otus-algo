import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        //var array = new int[] {3, 2, 1, 0, 5, 4};
        var array = new int[] {931, 827, 600, 89, 101, 821, 5};
        var out = new int[array.length];
        System.out.println(Arrays.toString(array));
        //CounterSort(array, out, 5);
        RadixSort(array, 3);
        System.out.println(Arrays.toString(array));
    }

    public static void CounterSort(int[] array, int[] sorted, int max_value) {
        var counter = new int[max_value + 1];
        Arrays.fill(sorted, 0);
        for(int i = 0; i < array.length; i++) {
            counter[array[i]]++;
        }

        for(int i = 1; i < max_value + 1; i++) {
            counter[i] = counter[i] + counter[i - 1];
        }

        for(int i = array.length - 1; i >= 0; i--) {
            counter[array[i]]--;
            sorted[counter[array[i]]] = array[i];
        }
    }

    public static void RadixSort(int[] array, int digits) {
        var counter = new int[10];
        var copy = new int[array.length];
        var value = 0;
        for(int j = 1, radix = 1; j <= digits; j++, radix *= 10) {
            Arrays.fill(counter, 0);
            System.arraycopy(array, 0, copy, 0, copy.length);

            for(int i = 0; i < copy.length; i++) {
                value = copy[i] % (radix * 10);
                value = value / radix;
                counter[value]++;
            }

            for(int i = 1; i < 10; i++) {
                counter[i] = counter[i] + counter[i - 1];
            }

            for(int i = copy.length - 1; i >= 0; i--) {
                value = copy[i] % (radix * 10);
                value = value / radix;
                counter[value]--;
                array[counter[value]] = copy[i];
            }
        }
    }
}
