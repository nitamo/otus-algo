class BloomFilter {
    private int[] bits;
    private int size;
    private HashStringFunction[] hashFunctions;

    private boolean testBits(int hash) {
        return ((bits[hash / 32] >> (hash % 32)) & 1) == 1;
    }

    private void setBits(int hash) {
        bits[hash / 32] |= 1 << hash % 32;
    }

    BloomFilter(int maxSize, float errorProbability) {
        var bitsEnough = -(maxSize * Math.log(errorProbability)) / (Math.log(2) * Math.log(2));
        int count = (int)((bitsEnough / maxSize) * Math.log(2));

        bitsEnough = Math.round(bitsEnough);
        count = Math.round(count);

        size = (int)bitsEnough;
        bits = new int[size / 32 + 1];

        hashFunctions = new HashStringFunction[count];
        for (int i = 0; i < count; i++) {
            hashFunctions[i] = HashFunctionFactory.getHashFunction();
        }
    }

    void add(String str) {
        for (var hashFunction : hashFunctions) {
            setBits(hashFunction.hashOf(str) % size);
        }
    }

    boolean test(String str) {
        for (var hashFunction : hashFunctions) {
            if(!testBits(hashFunction.hashOf(str) % size)) {
                return false;
            }
        }
        return true;
    }
}
