public class Main {

    public static void main(String[] args) {
        var filter = new BloomFilter(100, 0.1f);
        filter.add("apple");
        filter.add("orange");
        System.out.println(filter.test("apple"));
        System.out.println(filter.test("peach"));
        System.out.println(filter.test("orange"));
        System.out.println(filter.test("plum"));

        var minHash = new MinHash(0.1f);
        var set1 = new String[] {"apple", "orange"};
        var set2 = new String[] {"apple", "orange"};
        var set3 = new String[] {"apple", "peach"};
        var set4 = new String[] {"peach", "plum"};

        printMinHash(minHash, set1, set2);
        printMinHash(minHash, set1, set3);
        printMinHash(minHash, set1, set4);
        printMinHash(minHash, set2, set3);
        printMinHash(minHash, set2, set4);
        printMinHash(minHash, set3, set4);

        var text1 = "Hello world of shingles!";
        var text2 = "Hello morld of shingles!";
        var simHash = new SimHash(5);
        var distance = simHash.getDistance(text1, text2);
        System.out.println(distance);
    }

    static void printMinHash(MinHash minHash, String[] set1, String[] set2) {
        System.out.println("[" + set1[0] + ":"  + set1[1] + "][" + set2[0] + ":" + set2[1] + "]\t"
                + minHash.getSimilarity(minHash.getSignatures(set1), minHash.getSignatures(set2)));
    }
}
