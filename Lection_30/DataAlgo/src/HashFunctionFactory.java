class HashFunctionFactory {
    static HashStringFunction getHashFunction() {
        return new HashStringFunction() {
            int seed = (int)(Math.random() * 32) + 32;

            @Override
            public int hashOf(String str) {
                int result = 1;
                for (int i = 0; i < str.length(); i++) {
                    result = (seed * result + str.charAt(i));
                }
                return Math.abs(result);
            }
        };
    }
}
