public interface HashStringFunction {
    int hashOf(String str);
}
