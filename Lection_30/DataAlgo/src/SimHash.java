import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import java.nio.charset.Charset;

class SimHash {
    private int shingleSize;
    private HashFunction hashFunction;

    SimHash(int shingleSize) {
        this.shingleSize = shingleSize;
        hashFunction = Hashing.murmur3_32();
    }

    private String[] makeShingles(String str) {
        int shinglesCount = str.length() / shingleSize;
        shinglesCount = (str.length() % shingleSize == 0) ? shinglesCount : shinglesCount + 1;
        var result = new String[shinglesCount];
        var shingle = new char[shingleSize];
        for (int i = 0, sh = 0; i < str.length(); i += shingleSize, sh++) {
            for (int j = 0; j < shingleSize; j++) {
                //Fill remaining shingle with spaces if it's last one.
                if(i + j >= str.length()) {
                    while (j < shingleSize) {
                        shingle[j] = ' ';
                        j++;
                    }
                    break;
                }
                shingle[j] = str.charAt(i + j);
            }
            result[sh] = new String(shingle);
        }
        return result;
    }

    private int[] makeHashes(String[] shingles) {
        var result = new int[shingles.length];
        for (int i = 0; i < shingles.length; i++) {
            result[i] = hashFunction.hashString(shingles[i], Charset.defaultCharset()).asInt();
        }
        return result;
    }

    int getSimHash(String phrase) {
        int result = 0;
        int[] registers = new int[32];

        var shingles = makeShingles(phrase);
        var hashes = makeHashes(shingles);

        for (int hash : hashes) {
            for (int j = 0; j < 32; j++) {
                if ((hash & 1 << j) != 0) {
                    registers[j]++;
                } else {
                    registers[j]--;
                }
            }
        }

        for (int i = 0; i < 32; i++) {
            if(registers[i] > 0) {
                result |= (1 << i);
            }
        }

        return result;
    }

    int getDistance(String text1, String text2) {
        int result = 0;
        var hash1 = getSimHash(text1);
        var hash2 = getSimHash(text2);

        var diff = hash1 ^ hash2;

        for (int i = 0; i < 32; i++) {
            if((diff & 1 << i) != 0) {
                result++;
            }
        }

        return result;
    }
}
