import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import java.nio.charset.Charset;

class MinHash {
    private HashFunction[] hashFunctions;

    MinHash(float maxError) {
        var functionsCount = Math.round(1 / (maxError * maxError));
        hashFunctions = new HashFunction[functionsCount];
        for (int i = 0; i < functionsCount; i++) {
            hashFunctions[i] = Hashing.murmur3_32(i);
        }
    }

    private int findMinimum(String[] set, HashFunction hashFunction) {
        var minimum = Integer.MAX_VALUE;
        for (String string : set) {
            var hash = hashFunction.hashString(string, Charset.defaultCharset()).asInt();
            minimum = (hash < minimum) ? hash : minimum;
        }
        return minimum;
    }

    int[] getSignatures(String[] set) {
        var result = new int[hashFunctions.length];
        for (int i = 0; i < hashFunctions.length; i++) {
            result[i] = findMinimum(set, hashFunctions[i]);
        }
        return result;
    }

    float getSimilarity(int[] signaturesLeft, int[] signaturesRight) {
        var equalsCount = 0;
        for (int i = 0; i < signaturesLeft.length; i++) {
            if(signaturesLeft[i] == signaturesRight[i]) {
                equalsCount++;
            }
        }
        return (float) equalsCount / hashFunctions.length;
    }
}
