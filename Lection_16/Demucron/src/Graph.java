import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

class Graph<T> {
    final int defaultNodesCount = 8;
    int maxEdges = 3;
    List<T> nodes;
    int[][] matrix = new int[defaultNodesCount][maxEdges];

    public Graph() {
        matrix[0] = new int[] {1, -1, -1};
        matrix[1] = new int[] {2, 4, 5};
        matrix[2] = new int[] {3, 6, -1};
        matrix[3] = new int[] {2, 7, -1};
        matrix[4] = new int[] {0, 5, -1};
        matrix[5] = new int[] {6, -1, -1};
        matrix[6] = new int[] {5, -1, -1};
        matrix[7] = new int[] {3, 6, -1};

        nodes = new ArrayList<>(defaultNodesCount);
        for (int i = 0; i < defaultNodesCount; i++) {
            set(null, i);
        }
    }

    public Graph(int[][] matrix, int maxEdges) {
        this.matrix = matrix;
        this.maxEdges = maxEdges;
        this.nodes = new ArrayList<>(matrix.length);

    }

    public void set(T value, int index) {
        nodes.add(index, value);
    }

    public T get(int index) {
        return nodes.get(index);
    }

    public int size() {
        return matrix.length;
    }

    @Override
    public String toString() {
        var result = "";

        for (int i = 0; i < matrix.length; i++) {
            var vertex = matrix[i];
            result += "{" + i + ":" + nodes.get(i) + "}{";
            for (int j = 0; j < maxEdges; j++) {
                if(j > 0) {
                    result += ", ";
                }

                result += vertex[j];
                result += ":";

                if(vertex[j] != -1) {
                    result += nodes.get(vertex[j]);
                }
                else {
                    result += "_";
                }
            }
            result += "}\n";
        }
        return result;
    }

    private boolean[][] createAdjacencyMatrix() {
        var dimSize = matrix.length;
        var result = new boolean[dimSize][dimSize];

        for (int i = 0; i < dimSize; i++) {
            for (int j = 0; j < maxEdges && matrix[i][j] != -1; j++) {
                result[i][matrix[i][j]] = true;
            }
        }
        return result;
    }

    private int[] createWeightsList(boolean[][] adjacencyMatrix) {
        var dimSize = matrix.length;
        var result = new int[dimSize];
        for (int i = 0; i < dimSize; i++) {
            for (int j = 0; j < dimSize; j++) {
                if(adjacencyMatrix[i][j]) {
                    result[j]++;
                }
            }
        }
        return result;
    }

    public int[][] getTopSorted() {
        var dimSize = matrix.length;

        var am = createAdjacencyMatrix();
        var wl = createWeightsList(am);

        int max = 0;
        for (int i = 0; i < wl.length; i++) {
            if(wl[i] > max) {
                max = wl[i];
            }
        }
        max++;

        var tempResult = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < max; i++) {
            tempResult.add(new ArrayList<>());
        }

        for (int i = 0; i < max; i++) {
            var resultRow = tempResult.get(i);
            for (int j = 0; j < wl.length; j++) {
                if(wl[j] == 0) {
                    resultRow.add(j);
                    wl[j] = -1;
                }
            }
            for (int j = 0; j < resultRow.size(); j++) {
                var index = resultRow.get(j);
                for (int k = 0; k < dimSize; k++) {
                    if(am[index][k]) {
                        wl[k]--;
                    }
                }
            }
        }
        int[][] result = new int[tempResult.size()][];
        for (int i = 0; i < tempResult.size(); i++) {
            var row = tempResult.get(i);
            result[i] = new int[row.size()];
            for (int j = 0; j < row.size(); j++) {
                result[i][j] = row.get(j);
            }
        }
        return result;
    }
}