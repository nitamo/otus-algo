import java.util.Arrays;

public class Demucron {
    public static void main(String[] args) {
        var testMatrix = new int[] [] {
                {2, 12, -1, -1},
                {12, -1, -1, -1},
                {-1, -1, -1, -1},
                {2, -1, -1, -1},
                {2, 8, 9, -1},
                {3, 10, 11, 12},
                {5, -1, -1, -1},
                {1, 3, 5, 13},
                {0, 6, -1, -1},
                {0, 11, 13, -1},
                {2, -1, -1, -1},
                {-1, -1, -1, -1},
                {2, -1, -1, -1},
                {10, -1, -1, -1}
                };
        var graph = new Graph<String>(testMatrix, 4);
        for (int i = 0; i < testMatrix.length; i++) {
            graph.set("V" + (i + 1), i);
        }

        var sortedResult = graph.getTopSorted();
        for (var row: sortedResult) {
            for (var item: row) {
                System.out.print(item + " ");
            }
            System.out.println();
        }
    }
}
