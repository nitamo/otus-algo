public class Main {

    public static void main(String[] args){
        var timedCache = new TimedCache<Integer, Integer>(2, 1000);

        timedCache.set(1, 1);
        timedCache.set(10, 10);

        timedCache.set(100, 100);
        timedCache.set(1000, 1000);

        timedCache.set(10_000, 10_000);
        timedCache.set(100_000, 100_000);

        var temp = timedCache.get(10_000);
        timedCache.get(10);
        System.out.println(temp);
    }
}
