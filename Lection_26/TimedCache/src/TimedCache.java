import java.lang.ref.WeakReference;
import java.util.AbstractMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

class TimedCache<K, V> {
    private final Map<K, Entry<Long, V>> cache;
    private final ConcurrentNavigableMap<Long, K> times;
    private final int capacity;
    private final long expirationTime;

    TimedCache(int capacity, long expirationTimeMs) {
        if(capacity <= 0 || expirationTimeMs <= 0) {
            throw new IllegalArgumentException();
        }
        this.capacity = capacity;
        this.expirationTime = expirationTimeMs * 1_000_000;
        cache = new ConcurrentHashMap<>(capacity);
        times = new ConcurrentSkipListMap<>();

        var timedCacheWeakReference = new WeakReference<TimedCache>(this);
        var cleanupThread = new Thread(() -> {
            var timedCache = timedCacheWeakReference.get();
            while(timedCache != null) {
                timedCache.cleanupOldItems();
                try {
                    Thread.sleep(expirationTimeMs);
                }
                catch (InterruptedException ex) {
                    return;
                }
                timedCache = timedCacheWeakReference.get();
            }
        });
        cleanupThread.setDaemon(true);
        cleanupThread.start();
    }

    void set(K key, V value) {
        if(cache.size() >= capacity) {
            //Remove the eldest item
            var oldEntry = times.firstEntry();
            cache.remove(oldEntry.getValue());
            times.remove(oldEntry.getKey());
        }
        var time = System.nanoTime();
        cache.put(key, new Entry<>(time, value));
        times.put(time, key);
    }

    V get(K key) {
        var entry = cache.get(key);
        if (entry == null) {
            return null;
        }
        times.remove(entry.getKey());
        var time = System.nanoTime();
        cache.replace(key, new Entry<>(time, entry.getValue()));
        times.put(time, key);

        return entry.getValue();
    }

    private void cleanupOldItems() {
        //Guaranteed to work for 292 years without stop :)
        var oldItems = times.headMap(System.nanoTime() - expirationTime, true).entrySet();
        oldItems.forEach((entry) -> {
            cache.remove(entry.getValue());
            times.remove(entry.getKey());
        });
    }

    private static class Entry<T, V> extends AbstractMap.SimpleImmutableEntry<T, V> {

        Entry(T key, V value) {
            super(key, value);
        }
    }
}
