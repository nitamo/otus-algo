#include <iostream>
#include <chrono>
#include <ctime>
#include <thread>
#include "my_alloc.h"

const static size_t HEAP_SIZE = 0x10000000L; //256 MB

long test_allocation_time(std::size_t block_size, std::size_t allocations_count, std::size_t alloc_size) {
    auto start = std::chrono::system_clock::now();
    auto start_time_t = std::chrono::system_clock::to_time_t(start);
    std::cout << std::ctime(&start_time_t) << std::endl;

    auto heap_p = new char[HEAP_SIZE];

    mysetup(heap_p, HEAP_SIZE, block_size);

    for (std::size_t i = 0; i < allocations_count; i++) {
        myalloc(alloc_size);
    }

    delete[] heap_p;

    auto end = std::chrono::system_clock::now();
    auto end_time_t = std::chrono::system_clock::to_time_t(end);

    std::chrono::duration<double> elapsed_seconds = end - start;
    return elapsed_seconds.count();
}

void test_multithreaded() {
    auto heap_p = new char[HEAP_SIZE];

    mysetup(heap_p, HEAP_SIZE);

    auto test_process = []() {
        auto ch_p1 = (char*)myalloc(sizeof(char) * 4);
        auto ch_p2 = (char*)myalloc(sizeof(char) * 4);
        std::cout << (long)ch_p1 << std::endl;
        std::cout << (long)ch_p2 << std::endl;
        myfree(ch_p1);
        myfree(ch_p2);
    };

    std::thread t1(test_process);
    std::thread t2(test_process);

    t1.join();
    t2.join();

    delete[] heap_p;
}

int main() {
    std::cout << "My allocator testing!" << std::endl;

    auto heap_p = new char[HEAP_SIZE];

    mysetup(heap_p, HEAP_SIZE, 4L);
    auto ch_p1 = (char*)myalloc(sizeof(char) * 4);
    auto ch_p2 = (char*)myalloc(sizeof(char) * 4);

    std::cout << (long)ch_p1 << std::endl;
    std::cout << (long)ch_p2 << std::endl;

    myfree(ch_p1);
    myfree(ch_p2);

    delete[](heap_p);

    test_multithreaded();

    std::cout << "1; 100000" << std::endl;
    std::cout << test_allocation_time(1L, 100000, 1L) << std::endl;
    std::cout << "4; 100000" << std::endl;
    std::cout << test_allocation_time(4L, 100000, 1L) << std::endl;
    std::cout << "8; 100000" << std::endl;
    std::cout << test_allocation_time(8L, 100000, 1L) << std::endl;
    std::cout << "16; 100000" << std::endl;
    std::cout << test_allocation_time(16L, 100000, 1L) << std::endl;
    std::cout << "1; 200000" << std::endl;
    std::cout << test_allocation_time(1L, 200000, 1L) << std::endl;
    std::cout << "4; 200000" << std::endl;
    std::cout << test_allocation_time(4L, 200000, 1L) << std::endl;
    std::cout << "8; 200000" << std::endl;
    std::cout << test_allocation_time(8L, 200000, 1L) << std::endl;
    std::cout << "16; 200000" << std::endl;
    std::cout << test_allocation_time(16L, 200000, 1L) << std::endl;
    std::cout << "1; 400000" << std::endl;
    std::cout << test_allocation_time(1L, 400000, 1L) << std::endl;
    std::cout << "4; 400000" << std::endl;
    std::cout << test_allocation_time(4L, 400000, 1L) << std::endl;
    std::cout << "8; 400000" << std::endl;
    std::cout << test_allocation_time(8L, 400000, 1L) << std::endl;
    std::cout << "16; 400000" << std::endl;
    std::cout << test_allocation_time(16L, 400000, 1L) << std::endl;

    return 0;
}

