#include <cstring>
#include <thread>
#include <mutex>
#include "my_alloc.h"

static void* buffer_p = nullptr;
static std::size_t current_block_size = 1L;

static std::mutex allocator_mutex;

struct Node {
    Node* next;
    std::size_t size;
    bool free;
};

inline std::size_t calculateBlockNeededSize(std::size_t size) {
    std::size_t needed_size = size + sizeof(Node);
    std::size_t blocks = needed_size / current_block_size;
    if (needed_size % current_block_size != 0) {
        blocks++;
    }
    return blocks * current_block_size;
}

void mysetup(void *buf, std::size_t size, std::size_t block_size)
{
    std::lock_guard<std::mutex> guard(allocator_mutex);

    buffer_p = buf;
    Node node = {(Node*)buffer_p, size - sizeof(Node), true};
    std::memcpy(buffer_p, &node, sizeof(Node));
    current_block_size = block_size;
}

// Функция аллокации
void *myalloc(std::size_t size)
{
    Node* node_p = (Node*) buffer_p;
    std::size_t needed_size = calculateBlockNeededSize(size);

    if(needed_size < size)
        return NULL;

    std::lock_guard<std::mutex> guard(allocator_mutex);

    do {
        if(needed_size < node_p->size && node_p->free) {
            Node next_node = {(Node*)buffer_p, node_p->size - needed_size, true};
            Node* next_node_addr = (Node*)((char*)node_p + needed_size);
            std::memcpy(next_node_addr, &next_node, sizeof(Node));
            node_p->next = next_node_addr;
            node_p->free = false;
            node_p->size = size;
            return (void*)(node_p + 1);
        }
        node_p = node_p->next;
    } while (node_p != buffer_p);

    return NULL;
}

// Функция освобождения
void myfree(void *p)
{
    if(!p)
        return;

    std::lock_guard<std::mutex> guard(allocator_mutex);

    Node* current_node_p = (Node*)p - 1;
    Node* next_node_p = current_node_p->next == buffer_p ? NULL : current_node_p->next;

    current_node_p->free = true;
    if(next_node_p && next_node_p->free){
        current_node_p->next = next_node_p->next;
        current_node_p->size = current_node_p->size + next_node_p->size + sizeof(Node);
    }

    if(current_node_p == buffer_p)
        return;

    Node* previous_node_p = (Node*)buffer_p;
    while(previous_node_p->next != current_node_p)
        previous_node_p = previous_node_p->next;

    if(previous_node_p->free) {
        previous_node_p->next = current_node_p->next;
        previous_node_p->size = previous_node_p->size + current_node_p->size + sizeof(Node);
    }
}