import java.util.*;

class Graph<T> {
    int maxEdges;
    List<T> nodes;
    int[][] matrix;
    int[][] edges;

    public Graph(int[][] matrix, int maxEdges) {
        this.matrix = matrix;
        this.maxEdges = maxEdges;
        this.nodes = new ArrayList<>(matrix.length);
        this.vertices = new ArrayList<>(matrix.length);
        this.visitedNodes = new boolean[matrix.length];
    }

    public Graph(int[][] matrix, int maxEdges, int[][] edges) {
        this(matrix, maxEdges);
        this.edges = edges;
    }

    public void set(T value, int index) {
        nodes.add(index, value);
    }

    public T get(int index) {
        return nodes.get(index);
    }

    public int size() {
        return matrix.length;
    }

    @Override
    public String toString() {
        var result = "";

        for (int i = 0; i < matrix.length; i++) {
            var vertex = matrix[i];
            result += "{" + i + ":" + nodes.get(i) + "}{";
            for (int j = 0; j < maxEdges; j++) {
                if(j > 0) {
                    result += ", ";
                }

                result += vertex[j];
                result += ":";

                if(vertex[j] != -1) {
                    result += nodes.get(vertex[j]);
                }
                else {
                    result += "_";
                }
            }
            result += "}\n";
        }
        return result;
    }

    public static class Edge {
        public int v1;
        public int v2;

        public Edge(int v1, int v2) {
            this.v1 = v1;
            this.v2 = v2;
        }

        @Override
        public String toString() {
            return "{" + v1 + ":" + v2 + "}";
        }
    }

    private class Vertex {
        public int index;
        public int weight;
        public int prevVerIndex;

        Vertex(int index, int weight, int prevVerIndex) {
            this.index = index;
            this.weight = weight;
            this.prevVerIndex = prevVerIndex;
        }
    }

    private List<Vertex> vertices;

    private Deque<Vertex> activeVertexes = new ArrayDeque<>();

    boolean[] visitedNodes;

    public Edge[] getShortestPath(int firstIndex, int lastIndex) {

        for (int i = 0; i < matrix.length; i++) {
            vertices.add(null);
        }

        var initialVertex = new Vertex(firstIndex, 0, -1);
        activeVertexes.add(initialVertex);
        vertices.set(firstIndex, initialVertex);

        while (activeVertexes.size() != 0) {
            var current = activeVertexes.poll();
            var currentIndex = current.index;
            visitedNodes[currentIndex] = true;
            for(int i = 0; i < edges.length; i++) {
                if(edges[i][0] == currentIndex) {
                    var siblingIndex = edges[i][1];
                    if(!visitedNodes[siblingIndex]) {
                        var edgeWeight = edges[i][2];
                        var totalWeight = edgeWeight + current.weight;
                        var sibling = vertices.get(siblingIndex);
                        if(sibling != null) {
                            if(sibling.weight > totalWeight) {
                                sibling.weight = totalWeight;
                                sibling.prevVerIndex = currentIndex;
                            }
                        }
                        else {
                            var newSibling = new Vertex(siblingIndex, totalWeight, currentIndex);
                            activeVertexes.add(newSibling);
                            vertices.set(siblingIndex, newSibling);
                        }
                    }
                }
            }
        }

        var resultingList = new ArrayDeque<Edge>();
        int index = lastIndex;
        while(index != firstIndex) {
            var vertex = vertices.get(index);
            if(vertex == null) {
                return null;
            }
            resultingList.addFirst(new Edge(vertex.prevVerIndex, vertex.index));
            index = vertex.prevVerIndex;
        }

        return resultingList.toArray(new Edge[0]);
    }


}
