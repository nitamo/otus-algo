import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        var tree = new RBTree<Integer, Integer>(50, 50);
//        var arr = new int[] {25, 75, 12, 18, 10, 15};
//        var arr = new int[] {12, 75, 85, 95, 100, 82, 92};
        var arr = new int[] {5, 20, 1, 6, 7, 15, 25, 0, 2, 14, 16, 21, 26};
        Arrays.stream(arr).forEach((it) -> tree.insert(it, it));
        tree.printTree();
        return;
    }
}

class RBTree<K extends Comparable<K>, V> {
    private RBTree parent;
    private RBTree left;
    private RBTree right;
    private boolean isBlack;
    private K key;
    private V value;

    private RBTree(K key, V value, RBTree parent) {
        this.parent = parent;
        if(parent == null) {
            isBlack = true;
        }
        this.key = key;
        this.value = value;
    }

    public RBTree(K key, V value) {
        this(key, value, null);
    }

    private boolean isBlack() {
        return isBlack;
    }

    @SuppressWarnings("unchecked")
    public V getValue(K key) {
        var node = findNode(key);
        return (node != null) ? (V)node.value : null;
    }

    @SuppressWarnings("unchecked")
    public void insert(K newKey, V newValue) {
        if(newKey.equals(key)) {
            value = newValue;
            return;
        }

        checkChildrenColor();

        checkParentAndTurn();

        if(moreThan(newKey)) {
            if(right == null) {
                right = new RBTree(newKey, newValue, this);
                right.checkParentAndTurn();
            }
            else {
                right.insert(newKey, newValue);
            }
        }
        else {
            if(left == null) {
                left = new RBTree(newKey, newValue, this);
                left.checkParentAndTurn();
            }
            else {
                left.insert(newKey, newValue);
            }
        }

        checkParentAndTurn();
    }

    private void checkParentAndTurn() {
        if(!isBlack && parent != null && !parent.isBlack) {
            turnNode();
        }
    }

    private void checkChildrenColor() {
        if(isBlack && left != null && !left.isBlack && right != null && !right.isBlack) {
            isBlack = (parent == null);
            left.isBlack = true;
            right.isBlack = true;
        }
    }

    private void turnNode() {
        if(parent.parent.left == parent) {
            if(parent.left == this) {
                parent.parent.isBlack = false;
                parent.isBlack = true;
                parent.parent.SmallRightTurn();
                parent.checkChildrenColor();

            } else {
                var g = parent.parent;
                g.isBlack = false;
                isBlack = true;
                g.left.SmallLeftTurn();
                g.SmallRightTurn();
                g.checkChildrenColor();
            }
        } else {
            if(parent.left == this) {
                var g = parent.parent;
                g.isBlack = false;
                isBlack = true;
                g.right.SmallRightTurn();
                g.SmallLeftTurn();
                g.checkChildrenColor();
            } else {
                parent.parent.isBlack = false;
                parent.isBlack = true;
                parent.parent.SmallLeftTurn();
                parent.checkChildrenColor();
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void SmallRightTurn() {
        System.out.println("Small right turn " + key);
        var newNode = new RBTree<>(key, value);

        newNode.parent = this;
        newNode.isBlack = isBlack;

        if(right != null) {
            newNode.right = right;
            newNode.right.parent = newNode;
        }

        right = newNode;

        if(left.right != null) {
            left.right.parent = newNode;
            newNode.left = left.right;
        }

        key = (K)left.key;
        value = (V)left.value;
        isBlack = left.isBlack;
        left.parent = null;

        if(left.left != null) {
            left.left.parent = this;
        }

        left = left.left;
    }

    @SuppressWarnings("unchecked")
    private void SmallLeftTurn() {
        System.out.println("Small left turn " + key);
        var newNode = new RBTree(this.key, this.value);

        newNode.parent = this;
        newNode.isBlack = isBlack;

        if(left != null) {
            newNode.left = left;
            newNode.left.parent = newNode;
        }

        left = newNode;

        if(right.left != null) {
            right.left.parent = newNode;
            newNode.right = right.left;
        }

        key = (K)right.key;
        value = (V)right.value;
        isBlack = right.isBlack;
        right.parent = null;

        if(right.right != null) {
            right.right.parent = this;
        }

        right = right.right;
    }

    @SuppressWarnings("unchecked")
    private RBTree findNode(K key) {
        if(this.key.equals(key)) {
            return this;
        }
        if(moreThan(key) && right != null) {
            return right.findNode(key);
        }
        if(left != null) {
            return left.findNode(key);
        }

        return null;
    }

    private boolean moreThan(K key) {
        return (this.key.compareTo(key) < 0);
    }

    public void printTree() {
        var top = this;
        while(top.parent != null) {
            top = top.parent;
        }

        printTreeHelper(top, false);
        System.out.println();
    }

    private static void printTreeHelper(RBTree node, boolean left) {
        System.out.println("" + node.key + "\t"
                + ((left) ? "/" : "\\") + "\t"
                + ((node.isBlack) ? "black" : "red  ") + "\t\t"
                + node.hashCode() + "\t"
                + ((node.parent != null) ? "" + node.parent.key + "\t" + node.parent.hashCode() : ""));

        if(node.left != null) {
            printTreeHelper(node.left, true);
        }
        if(node.right != null) {
            printTreeHelper(node.right, false);
        }
    }
}
