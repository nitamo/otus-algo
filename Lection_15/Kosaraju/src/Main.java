import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        var g = new Graph<String>();
        for (int i = 0; i < g.size(); i++) {
            g.set("" + (char)('A' + i), i);
        }

        System.out.println(g.depthFirstSearch("H"));

        var groups = g.kosaraju();
        System.out.println(Arrays.toString(groups));
    }
}
