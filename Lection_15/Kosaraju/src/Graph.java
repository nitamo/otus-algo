import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

class Graph<T> {
    final int defaultNodesCount = 8;
    int maxEdges = 3;
    List<T> nodes = new ArrayList<>(defaultNodesCount);
    int[][] matrix = new int[defaultNodesCount][maxEdges];

    public Graph() {
        matrix[0] = new int[] {1, -1, -1};
        matrix[1] = new int[] {2, 4, 5};
        matrix[2] = new int[] {3, 6, -1};
        matrix[3] = new int[] {2, 7, -1};
        matrix[4] = new int[] {0, 5, -1};
        matrix[5] = new int[] {6, -1, -1};
        matrix[6] = new int[] {5, -1, -1};
        matrix[7] = new int[] {3, 6, -1};
    }

    public Graph(int[][] matrix, int maxEdges) {
        this.matrix = matrix;
        this.maxEdges = maxEdges;
        nodes = new ArrayList<>(matrix.length);
    }

    public void set(T value, int index) {
        nodes.add(index, value);
    }

    public T get(int index) {
        return nodes.get(index);
    }

    public int size() {
        return matrix.length;
    }

    @Override
    public String toString() {
        var result = "";

        for (int i = 0; i < matrix.length; i++) {
            var vertex = matrix[i];
            result += "{" + i + ":" + nodes.get(i) + "}{";
            for (int j = 0; j < maxEdges; j++) {
                if(j > 0) {
                    result += ", ";
                }

                result += vertex[j];
                result += ":";

                if(vertex[j] != -1) {
                    result += nodes.get(vertex[j]);
                }
                else {
                    result += "_";
                }
            }
            result += "}\n";
        }
        return result;
    }

    private Graph invert() {
        int[][] newMatrix = new int[defaultNodesCount][maxEdges];
        for (var vertex : newMatrix) {
            for (int i = 0; i < vertex.length; i++) {
                vertex[i] = -1;
            }
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length && matrix[i][j] != -1; j++) {
                var targetVertexIndex = matrix[i][j];
                var targetVertexInfo = newMatrix[targetVertexIndex];
                for (int k = 0; k < targetVertexInfo.length; k++) {
                    if(targetVertexInfo[k] == -1) {
                        targetVertexInfo[k] = i;
                        break;
                    }
                }
            }
        }

        var result = new Graph();
        result.matrix = newMatrix;
        result.nodes = nodes;
        result.maxEdges = maxEdges;
        return result;
    }

    private Stack<Integer> path = new Stack<>();
    private boolean[] visitedNodes = new boolean[8];

    public T depthFirstSearch(T value) {
        path.clear();
        var index = dfsHelper(0, value);
        if(index != -1)
            return nodes.get(index);
        else
            return null;
    }

    private int dfsHelperRecursive(int index, T value) {
        visitedNodes[index] = true;
        path.push(index);

        for (int i = 0; i < matrix[index].length; i++) {
            var newNodeIndex = matrix[index][i];
            if(newNodeIndex == -1)
                break;
            if(nodes.get(newNodeIndex).equals(value)) {
                return newNodeIndex;
            }
            if(!visitedNodes[newNodeIndex]) {
                var foundIndex = dfsHelperRecursive(newNodeIndex, value);
                if(foundIndex != -1) {
                    return foundIndex;
                }
            }
        }
        path.pop();
        return -1;
    }

    private int dfsHelper(int index, T value) {
        visitedNodes[index] = true;
        path.push(index);

        while (!path.empty()) {
            var currentIndex = path.pop();
            if(nodes.get(currentIndex).equals(value)) {
                return currentIndex;
            }
            if(!visitedNodes[currentIndex]) {
                visitedNodes[currentIndex] = true;
            }
            for (int i = 0; i < matrix[currentIndex].length; i++) {
                var newNodeIndex = matrix[currentIndex][i];
                if(newNodeIndex == -1)
                    break;
                if(!visitedNodes[newNodeIndex]) {
                    path.push(newNodeIndex);
                }
            }
        }
        return -1;
    }

    public int[] kosaraju() {
        var invertedGraph = invert();
        var result = new int[matrix.length];

        for (int i = 0; i < invertedGraph.matrix.length; i++) {
            invertedGraph.dfsrKosaraju(i);
        }
        kosarajuPath = invertedGraph.kosarajuPath;

        int index = 0;
        Arrays.fill(visitedNodes, false);

        while(!kosarajuPath.empty()) {
            var currentNode = kosarajuPath.pop();
            if(!visitedNodes[currentNode]) {
                index++;
                dfsrKosaraju(currentNode);
                for (int i = 0; i < visitedNodes.length; i++) {
                    if(visitedNodes[i] == true && result[i] == 0) {
                        result[i] = index;
                    }
                }
            }
        }
        return result;
    }

    private Stack<Integer> kosarajuPath = new Stack<>();
    private int dfsrKosaraju(int index) {
        if(!visitedNodes[index]) {
            kosarajuPath.push(index);
        }
        else {
            return -1;
        }
        visitedNodes[index] = true;
        path.push(index);

        for (int i = 0; i < matrix[index].length; i++) {
            var newNodeIndex = matrix[index][i];
            if(newNodeIndex == -1)
                break;
            if(!visitedNodes[newNodeIndex]) {
                var foundIndex = dfsrKosaraju(newNodeIndex);
                if(foundIndex != -1) {
                    return foundIndex;
                }
            }
        }
        path.pop();
        return -1;
    }
}

