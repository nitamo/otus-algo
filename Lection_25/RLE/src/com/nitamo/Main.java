package com.nitamo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Wrong number of arguments.");
            System.out.println("Usage: app [-c/d] [file_name]");
            return;
        }

        boolean compress = false;
        try {
            compress = args[0].charAt(args[0].length() - 1) == 'c';
        } catch (Exception ex) {
            System.out.println("Invalid operation argument.");
            System.out.println("Usage: app [-c/d] [file_name]");
        }

        var inputBuffer = new byte[2]; //Especially small for additional testing
        try (var output = new FileOutputStream("output")) {
            try (var input = new FileInputStream(args[1])) {
                if(compress) {
                    compress(inputBuffer, output, input);
                }
                else {
                    decompress(inputBuffer, output, input);
                }
            }
            catch (Exception ex) {
                System.out.println("Cannot read input file.");
            }
        }
        catch (Exception ex) {
            System.out.println("Cannot write output file.");
        }
    }

    private static void compress(byte[] inputBuffer, FileOutputStream output, FileInputStream input) throws IOException {
        byte prevByte = 0;
        int copyBytesCounter = 0;
        var uniqueBytes = new byte[128];
        int uniqueBytesCounter = 0;
        for (var len = input.read(inputBuffer); len > 0; len = input.read(inputBuffer)) {
            for (int i = 0; i < len; i++) {
                if(copyBytesCounter == 0) {
                    copyBytesCounter++;
                    prevByte = inputBuffer[i];
                    uniqueBytes[uniqueBytesCounter] = inputBuffer[i];
                    uniqueBytesCounter++;
                    continue;
                }
                if(prevByte == inputBuffer[i]) {
                    if(uniqueBytesCounter > 0) { //Flush unique bytes
                        uniqueBytesCounter--; //Skip stored prevByte from uniqueBytes
                        flushUniqueBytes(output, uniqueBytes, uniqueBytesCounter);
                        uniqueBytesCounter = 0;
                    }

                    copyBytesCounter++;
                    if(copyBytesCounter > 0x7F) { //Compressed bytes counter overflow
                        output.write(0x7F);
                        output.write(prevByte);
                        copyBytesCounter = 1;
                    }
                }
                else {
                    if(copyBytesCounter > 1) { //Write non-unique byte info first
                        output.write(copyBytesCounter);
                        output.write(prevByte);
                    }

                    if(uniqueBytesCounter + 1 > 0x7F) { //Unique bytes counter overflow, flush first
                        flushUniqueBytes(output, uniqueBytes, uniqueBytesCounter);
                        uniqueBytesCounter = 0;
                    }
                    uniqueBytes[uniqueBytesCounter] = inputBuffer[i];
                    uniqueBytesCounter++;

                    copyBytesCounter = 1;
                    prevByte = inputBuffer[i];
                }
            }
        }
        //Write remaining data
        if(copyBytesCounter > 1) {
            output.write(copyBytesCounter);
            output.write(prevByte);
        }
        if(uniqueBytesCounter > 1) {
            flushUniqueBytes(output, uniqueBytes, uniqueBytesCounter);
        }
    }

    private static void flushUniqueBytes(FileOutputStream output, byte[] uniqueBytes, int uniqueBytesCounter) throws IOException {
        output.write(0x80 + uniqueBytesCounter);
        for (int j = 0; j < uniqueBytesCounter; j++) {
            output.write(uniqueBytes[j]);
        }
    }

    private static void decompress(byte[] inputBuffer, FileOutputStream output, FileInputStream input) throws IOException {
        int in;
        int copyBytesCounter = 0;
        int uniqueBytesCounter = 0;
        for (var len = input.read(inputBuffer); len > 0; len = input.read(inputBuffer)) {
            for (int i = 0; i < len; i++) {
                if(uniqueBytesCounter > 0) {
                    uniqueBytesCounter--;
                    output.write(inputBuffer[i]);
                    continue;
                }
                if (copyBytesCounter > 0) {
                    for (;copyBytesCounter > 0; copyBytesCounter--) {
                        output.write(inputBuffer[i]);
                    }
                    continue;
                }

                in = Byte.toUnsignedInt(inputBuffer[i]);
                if ((in & 0x80) == 0) { //Non-unique
                    copyBytesCounter = in;
                    i++;
                    if(i < inputBuffer.length) {
                        for (;copyBytesCounter > 0; copyBytesCounter--) {
                            output.write(inputBuffer[i]);
                        }
                    }
                } else { //Unique
                    uniqueBytesCounter = in & 0x7F;
                    i++;
                    if(i < inputBuffer.length) {
                        output.write(inputBuffer[i]);
                        uniqueBytesCounter--;
                    }
                }
            }
        }
    }
}
