import java.math.BigDecimal;

public class Fib {
    public static int FibRecursion(int number) {
        if(number < 2)
            return number;
        return FibRecursion(number - 1) + FibRecursion(number - 2);
    }

    public static BigDecimal FibIteration(int number) {

        if(number < 2)
            return new BigDecimal(number);

        var array = new BigDecimal[3];
        array[0] = new BigDecimal(0);
        array[1] = new BigDecimal(1);
        array[2] = new BigDecimal(1);

        for(int i = 2; i < number; i++) {
            BigDecimal temp = array[2];
            array[2] = array[2].add(array[1]);
            array[0] = array[1];
            array[1] = temp;
        }

        return array[2];
    }

    public static BigDecimal FibGR(long number) {
        double F = (Math.sqrt(5) + 1) / 2;
        return new BigDecimal((Math.pow(F, number) / Math.sqrt(5)) + 0.5d);
    }

    public static BigDecimal FibMatrix(long number) {

        if(number < 2)
            return new BigDecimal(number);

        var first = new MatrixF();
        var second = new MatrixF();

        for(long i = 1; i < number; i++) {
            first = MatrixF.mult(first, second);
        }
        return first.numbers[1];
    }

    public static BigDecimal FibMatrixEx(long number) {

        if(number < 2)
            return new BigDecimal(number);

        var result = (MatrixF)MatrixF.IDENTITY.clone();
        var base = (MatrixF)MatrixF.BASE.clone();

        while (number > 1) {
            if ((number & 1L) == 1)
                result = result.mult(base);
            base = base.mult(base);
            number >>= 1L;
        }
        result = result.mult(base);

        return result.numbers[1];
    }
}

class MatrixF implements Cloneable {
    public static final MatrixF IDENTITY = new MatrixF(new BigDecimal(1)
            , new BigDecimal(0)
            , new BigDecimal(1)
            , new BigDecimal(0));
    public static final MatrixF BASE = new MatrixF();

    public BigDecimal[] numbers = new BigDecimal[4];

    public MatrixF() {
        this(new BigDecimal(1)
                , new BigDecimal(1)
                , new BigDecimal(1)
                , new BigDecimal(0));
    }

    @Override
    protected Object clone() {
        var result = new MatrixF(this.numbers[0], this.numbers[1],
                this.numbers[2], this.numbers[3]);
        return result;
    }

    public MatrixF(BigDecimal i0, BigDecimal i1, BigDecimal i2, BigDecimal i3) {
        numbers[0] = i0; numbers[1] = i1;
        numbers[2] = i2; numbers[3] = i3;
    }

    public static MatrixF mult(MatrixF first, MatrixF second) {
        var result = new MatrixF();
        result.numbers[0] = (first.numbers[0].multiply(second.numbers[0]))
                .add(first.numbers[1].multiply(second.numbers[2]));
        result.numbers[1] = (first.numbers[0].multiply(second.numbers[1]))
                .add(first.numbers[1].multiply(second.numbers[3]));
        result.numbers[2] = (first.numbers[2].multiply(second.numbers[0]))
                .add(first.numbers[3].multiply(second.numbers[2]));
        result.numbers[3] = (first.numbers[2].multiply(second.numbers[1]))
                .add(first.numbers[3].multiply(second.numbers[3]));

        return result;
    }

    public MatrixF mult(MatrixF other) {
        var temp = new BigDecimal[4];
        temp[0] = (this.numbers[0].multiply(other.numbers[0]).add(this.numbers[1].multiply(other.numbers[2])));
        temp[1] = (this.numbers[0].multiply(other.numbers[1]).add(this.numbers[1].multiply(other.numbers[3])));
        temp[2] = (this.numbers[2].multiply(other.numbers[0]).add(this.numbers[3].multiply(other.numbers[2])));
        temp[3] = (this.numbers[2].multiply(other.numbers[1]).add(this.numbers[3].multiply(other.numbers[3])));

        this.numbers[0] = temp[0];
        this.numbers[1] = temp[1];
        this.numbers[2] = temp[2];
        this.numbers[3] = temp[3];
        return this;
    }
}
