public class EuclidGCD {
    public static long EuclidGCDSub(long a, long b) {
        while(a != b) {
            if(a > b) {
                a = a - b;
            }
            else {
                b = b - a;
            }
        }
        return a;
    }

    public static long EuclidGCDDiv(long a, long b) {
        while(a != 0 && b != 0) {
            if(a > b) {
                a = a % b;
            }
            else {
                b = b % a;
            }
        }
        return (a != 0) ? a : b;
    }
}
