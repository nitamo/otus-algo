import java.util.ArrayList;
import java.util.List;

public class PrimeNumbers {
    public static List<Integer> TrivialSearch(int limit) {
        var result = new ArrayList<Integer>();
        boolean is_prime = true;
        for(int i = 0; i <= limit; i++) {
            for(int j = 2; j*j <= i; j++) {
                if(i % j == 0) {
                    is_prime = false;
                    break;
                }
            }
            if(is_prime) {
                result.add(i);
            }
            is_prime = true;
        }
        return result;
    }

    public static List<Integer> SieveSearch(int limit) {
        var result = new ArrayList<Integer>();
        var array = new boolean[limit + 1];

        for(int i = 2; i * i <= limit; i++) {
            if(array[i]) {
                continue;
            }
            for(int j = i * i; j <= limit; j += i) {
                array[j] = true;
            }
        }


        for(int i = 0; i <= limit; i++) {
            if(!array[i]) {
                result.add(i);
            }
        }
        return result;
    }

    public static List<Integer> SieveSearchBitsArray(int limit) {
        var result = new ArrayList<Integer>();
        var size = limit / 32;
        ++size;
        var array = new int[size];

        for(int i = 2; i * i <= limit; i++) {
            int cell_i = i / 32;
            int offset_i = i % 32 - 1;

            if(((array[cell_i] >> offset_i) & 1) == 1) {
                continue;
            }
            for(int j = i * i; j <= limit; j += i) {
                int cell_j = j / 32;
                int offset_j = j % 32 - 1;
                array[cell_j] |= (1 << offset_j);
            }
        }

        for(int i = 0; i <= limit; i++) {
            int cell = i / 32;
            int offset = i % 32 - 1;

            if(((array[cell] >> offset) & 1) == 0) {
                result.add(i);
            }
        }

        return result;
    }

    public static List<Integer> SieveSearchBitsArraySkipEven(int limit) {
        var result = new ArrayList<Integer>();
        var size = limit / 64;
        ++size;
        var array = new int[size];

        for(int i = 0; i < 3 && i < limit; i++) {
            result.add(i);
        }

        for(int i = 3; i * i <= limit; i += 2) {
            int cell_i = i / 64;
            int offset_i = (i % 64) / 2 - 1;

            if(((array[cell_i] >> offset_i) & 1) == 1) {
                continue;
            }
            for(int j = i * i; j <= limit; j += i) {
                if(j % 2 == 0)
                    continue;
                int cell_j = j / 64;
                int offset_j = (j % 64) / 2 - 1;
                array[cell_j] |= (1 << offset_j);
            }
        }

        for(int i = 3; i <= limit; i += 2) {
            int cell = i / 64;
            int offset = (i % 64) / 2 - 1;

            if(((array[cell] >> offset) & 1) == 0) {
                result.add(i);
            }
        }

        return result;
    }
}
