public class FastPower {
    public static long PowMult(long a, long p) {
        long res = 1;
        for(; p > 0; p--) {
            res *= a;
        }
        return res;
    }

    public static long PowTwo(long a, long p) {
        long res = 1;
        long i = 1;
        long t = a;

        while(i < p / 2) {
            i <<= 1;
            res *= a;
            a *= a;
        }

        p -= i;

        for(; p >= 0; p--) {
            res *= t;
        }

        return res;
    }

    public static long PowBin(long a, long p) {
        long res = 1;
        while (p > 1) {
            if (p % 2 == 1)
                res *= a;
            a *= a;
            p /= 2;
        }
        if (p > 0)
            res *= a;

        return res;
    }
}
