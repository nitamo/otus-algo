import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        var matrix = new int[][] {
                {1, 3, -1, -1, -1},
                {0, 2, 3, 4, -1},
                {1, 4, -1, -1, -1},
                {0, 1, 4, 5, -1},
                {1, 2, 3, 5, 6},
                {3, 4, 6, -1, -1},
                {4, 5, -1, -1, -1}
        };

        var edges = new int[][] {
                {0, 1, 7},
                {0, 3, 5},
                {1, 2, 8},
                {1, 3, 9},
                {1, 4, 7},
                {2, 4, 5},
                {3, 4, 15},
                {3, 5, 6},
                {4, 5, 8},
                {4, 6, 9},
                {5, 6, 11}
        };

        var graph = new Graph<String>(matrix, 5, edges);

        for (int i = 0; i < matrix.length; i++) {
            graph.set(Character.toString('A' + i), i);
        }

        System.out.println(graph);

        var minSpanTree = graph.getMinSpanTree();

        System.out.println(Arrays.toString(minSpanTree));
    }
}
