import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Graph<T> {
    int maxEdges;
    List<T> nodes;
    int[][] matrix;
    int[][] edges;

    public Graph(int[][] matrix, int maxEdges) {
        this.matrix = matrix;
        this.maxEdges = maxEdges;
        this.nodes = new ArrayList<>(matrix.length);
    }

    public Graph(int[][] matrix, int maxEdges, int[][] edges) {
        this(matrix, maxEdges);
        this.edges = edges;
    }

    public void set(T value, int index) {
        nodes.add(index, value);
    }

    public T get(int index) {
        return nodes.get(index);
    }

    public int size() {
        return matrix.length;
    }

    @Override
    public String toString() {
        var result = "";

        for (int i = 0; i < matrix.length; i++) {
            var vertex = matrix[i];
            result += "{" + i + ":" + nodes.get(i) + "}{";
            for (int j = 0; j < maxEdges; j++) {
                if(j > 0) {
                    result += ", ";
                }

                result += vertex[j];
                result += ":";

                if(vertex[j] != -1) {
                    result += nodes.get(vertex[j]);
                }
                else {
                    result += "_";
                }
            }
            result += "}\n";
        }
        return result;
    }

    public static class Edge {
        public int v1;
        public int v2;

        public Edge(int v1, int v2) {
            this.v1 = v1;
            this.v2 = v2;
        }

        @Override
        public String toString() {
            return "{" + v1 + ":" + v2 + "}";
        }
    }

    private static class MergeFindSet {
        private int maxLevel;
        private int[] items;
        private int[] ranks;

        public MergeFindSet(int maxLevel) {
            this.maxLevel = maxLevel;
            items = new int[maxLevel];
            ranks = new int[maxLevel];
        }

        public void makeSet(int x) {
            items[x] = x;
            ranks[x] = 0;
        }

        private int find(int x) {
            if(x == items[x]) {
                return x;
            }
            items[x] = find(items[x]);
            return items[x];
        }

        public boolean union(int x, int y) {
            x = find(x);
            y = find(y);
            if(x == y) {
                return false;
            }

            if(ranks[x] < ranks[y]) {
                items[x] = y;
            }
            else {
                items[y] = x;
                if(ranks[x] == ranks[y]) {
                    ranks[x]++;
                }
            }
            return true;
        }
    }

    public Edge[] getMinSpanTree()
    {
        var tempResult = new ArrayList<Edge>();
        var mergeFindSet = new MergeFindSet(matrix.length);

        Arrays.sort(edges, (v1, v2) -> { return v1[2] - v2[2]; });

        for (int i = 0; i < matrix.length; i++) {
            mergeFindSet.makeSet(i);
        }

        for (var edge : edges) {
            if(mergeFindSet.union(edge[0], edge[1])) {
                tempResult.add(new Edge(edge[0], edge[1]));
            }
        }

        return tempResult.toArray(new Edge[0]);
    }
}
