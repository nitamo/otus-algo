import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        var root = new AvlTree<>(10, 10);

        var arr = new int[] {5, 20, 1, 6, 7, 15, 25, 0, 2, 14, 16, 21, 26};
        Arrays.stream(arr).forEach((it) -> root.insert(it, it));
        root.remove(20);
    }
}

class AvlTree<K extends Comparable<K>, V> {
    private AvlTree parent;
    private AvlTree left;
    private AvlTree right;
    private int height;
    private K key;
    private V value;

    public AvlTree(K key, V value, AvlTree parent) {
        this.parent = parent;
        this.key = key;
        this.value = value;
    }

    public AvlTree(K key, V value) {
        this(key, value, null);
    }

    @SuppressWarnings("unchecked")
    public V getValue(K key) {
        var node = findNode(key);
        return (node != null) ? (V)node.value : null;
    }

    @SuppressWarnings("unchecked")
    public void insert(K newKey, V newValue) {
        if(newKey.equals(key)) {
            value = newValue;
            return;
        }

        if(moreThan(newKey)) {
            if(right == null) {
                right = new AvlTree(newKey, newValue, this);
            }
            else {
                right.insert(newKey, newValue);
            }
        }
        else {
            if(left == null) {
                left = new AvlTree(newKey, newValue, this);
            }
            else {
                left.insert(newKey, newValue);
            }
        }
        renewHeight();
        balance();
    }

    @SuppressWarnings("unchecked")
    public V remove(K key) {
        var node = findNode(key);
        if(node == null) {
            return null;
        }

        V result = (V)node.value;
        if(node.parent != null) {
            if (node.moreThan(node.parent.key)) {
                if(node.right != null) {
                    node.parent.left = node.right;
                    node.right.parent = node.parent;
                    node.right.insertTree(node.left);
                } else {
                    node.parent.left = node.left;
                    if (node.left != null) {
                        node.left.parent = node.parent;
                    }
                }
            } else {
                if(node.left != null) {
                    node.parent.right = node.left;
                    node.left.parent = node.parent;
                    node.left.insertTree(node.right);
                } else {
                    node.parent.right = node.right;
                    if (node.right != null) {
                        node.right.parent = node.parent;
                    }
                }
            }

            node.parent.renewHeight();
            node.parent.balance();
            clearNode(node);
            return result;
        }
        else {
            if(node.left != null) {
                if(node.right != null) {
                    node.left.insertTree(node.right);
                    node.right = null;
                }
                var tmp = node.left;
                node.key = node.left.key;
                node.value = node.left.value;
                node.left = tmp.left;
                node.right = tmp.right;
                clearNode(tmp);

                renewHeight();
                balance();

                return result;
            }
            if(right != null) {
                var tmp = node.right;
                node.key = node.right.key;
                node.value = node.right.value;
                node.left = tmp.left;
                node.right = tmp.right;
                clearNode(tmp);
            }
        }

        renewHeight();
        balance();

        return result;
    }

    @SuppressWarnings("unchecked")
    private void insertTree(AvlTree subtree) {
        if(subtree == null) {
            return;
        }

        if(subtree.key.equals(key)) {
            throw new IllegalArgumentException();
        }

        if(moreThan((K)subtree.key)) {
            if(right == null) {
                subtree.parent = this;
                right = subtree;
                right.renewHeight();
            }
            else {
                right.insertTree(subtree);
            }
        }
        else {
            if(left == null) {
                subtree.parent = this;
                left = subtree;
                left.renewHeight();
            }
            else {
                left.insertTree(subtree);
            }
        }

        renewHeight();
        balance();
    }

    private int getBalance() {
        var lh = (left != null) ? left.height : 0;
        var rh = (right != null) ? right.height : 0;
        return lh - rh;
    }

    private void renewHeight() {
        if(left == null && right == null) {
            return;
        }
        var lh = (left != null) ? left.height : 0;
        var rh = (right != null) ? right.height : 0;
        height = (lh > rh) ? lh : rh;
        height++;
    }

    private void balance() {
        var balance = getBalance();
        if(-2 < balance && balance < 2) {
            return;
        }

        var lh = (left != null) ? left.height : 0;
        var rh = (right != null) ? right.height : 0;

        if(lh - rh >= 2) {
            var llh = (left.left != null) ? left.left.height : 0;
            var lrh = (left.right != null) ? left.right.height : 0;
            if(lrh > llh) {
                BigRightTurn();
            }
            else {
                SmallRightTurn();
            }
        }
        if(rh - lh >= 2) {
            var rlh = (right.left != null) ? right.left.height : 0;
            var rrh = (right.right != null) ? right.right.height : 0;
            if(rrh < rlh) {
                BigLeftTurn();
            }
            else {
                SmallLeftTurn();
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void SmallRightTurn() {
        System.out.println("Small right turn " + key);
        var newNode = new AvlTree(this.key, this.value);

        newNode.parent = this;

        if(right != null) {
            newNode.right = right;
            newNode.right.parent = newNode;
        }

        right = newNode;

        if(left.right != null) {
            left.right.parent = newNode;
            newNode.left = left.right;
        }

        newNode.renewHeight();

        key = (K)left.key;
        value = (V)left.value;
        left.parent = null;

        if(left.left != null) {
            left.left.parent = this;
        }

        left = left.left;

        renewHeight();
    }

    @SuppressWarnings("unchecked")
    private void SmallLeftTurn() {
        System.out.println("Small left turn " + key);
        var newNode = new AvlTree(this.key, this.value);

        newNode.parent = this;

        if(left != null) {
            newNode.left = left;
            newNode.left.parent = newNode;
        }

        left = newNode;

        if(right.left != null) {
            right.left.parent = newNode;
            newNode.right = right.left;
        }

        newNode.renewHeight();

        key = (K)right.key;
        value = (V)right.value;
        right.parent = null;

        if(right.right != null) {
            right.right.parent = this;
        }

        right = right.right;

        renewHeight();
    }

    private void BigRightTurn() {
        System.out.println("Big right turn " + key);

        SmallRightTurn();
        right.SmallRightTurn();
        SmallLeftTurn();
    }

    private void BigLeftTurn() {
        System.out.println("Big left turn " + key);

        SmallLeftTurn();
        left.SmallLeftTurn();
        SmallRightTurn();
    }

    @SuppressWarnings("unchecked")
    private AvlTree findNode(K key) {
        if(this.key.equals(key)) {
            return this;
        }
        if(moreThan(key) && right != null) {
            return right.findNode(key);
        }
        if(left != null) {
            return left.findNode(key);
        }

        return null;
    }

    private boolean moreThan(K key) {
        return (this.key.compareTo(key) < 0);
    }

    private void clearNode(AvlTree node) {
        node.key = null;
        node.value = null;
        node.parent = node.left = node.right = null;
    }
}