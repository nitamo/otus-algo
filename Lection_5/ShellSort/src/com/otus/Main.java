package com.otus;

import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        var array = new Random().longs(100, 1, 100).toArray();
        System.out.println(Arrays.toString(array));
        ShellSortSedgewickSequence(array);
        System.out.println(Arrays.toString(array));
        //testSort();
        System.out.println(GetSedgewickSequenceNumber(-1));
    }

    private static void InsertionSort(long[] array) {
        for(int i = 1; i < array.length; i++) {
            long t = array[i];
            for(int j = i; j > 0 && array[j - 1] > array[j]; j--) {
                array[j] = array[j-1];
                array[j-1] = t;
            }
        }
    }

    private static void ShellSort(long[] array, int initialGap) {
        for(int g = initialGap; g > 0; g--) {
            for(int k = 0; k < g; k++) {
                for(int i = g + k; i < array.length; i += g) {
                    for(int j = i; j > 0 && j - g >= 0 && array[j - g] > array[j]; j -= g) {
                        long t = array[j];
                        array[j] = array[j - g];
                        array[j - g] = t;
                    }
                }
            }
        }
    }

    private static void ShellSortEx(long[] array, int initialGap) {
        for(int g = initialGap; g > 0; g--) {
            for(int i = g; i < array.length; i++) {
                for(int j = i; j > g - 1 && array[j - g] > array[j]; j -= g) {
                    long t = array[j];
                    array[j] = array[j - g];
                    array[j - g] = t;
                }
            }
        }
    }

    private static void ShellSortSedgewickSequence(long[] array) {
        int n = -1;

        for(int limit = 0; limit < array.length; n++) {
            limit = GetSedgewickSequenceNumber(n);
        }

        for(int g = GetSedgewickSequenceNumber(n); n > -2; n--, g = GetSedgewickSequenceNumber(n)) {
            for(int i = g; i < array.length; i++) {
                for(int j = i; j > g - 1 && array[j - g] > array[j]; j -= g) {
                    long t = array[j];
                    array[j] = array[j - g];
                    array[j - g] = t;
                }
            }
        }
    }

    private static int GetSedgewickSequenceNumber(int n) {
        if(n < 0) {
            return 1;
        }
        return (int)(Math.pow(4, n + 1) + 3 * Math.pow(2, n) + 1);
    }

    private static void ShellSortGap4(long[] array) {
        ShellSortEx(array, 4);
    }

    private static void testSort() {

        var array100 = new long[100];
        var array1000 = new long[1000];
        var array10000 = new long[10000];
        var array100000 = new long[100000];

        var arrayR100 = new Random().longs(100).toArray();
        var arrayR1000 = new Random().longs(1000).toArray();
        var arrayR10000 = new Random().longs(10000).toArray();
        var arrayR100000 = new Random().longs(100000).toArray();

        var array10P100 = make10percentArray(array100, arrayR100);
        var array10P1000 = make10percentArray(array1000, arrayR1000);
        var array10P10000 = make10percentArray(array10000, arrayR10000);
        var array10P100000 = make10percentArray(array100000, arrayR100000);

        var array5E100 = make5elementsArray(array100, arrayR100);
        var array5E1000 = make5elementsArray(array1000, arrayR1000);
        var array5E10000 = make5elementsArray(array10000, arrayR10000);
        var array5E100000 = make5elementsArray(array100000, arrayR100000);

        var time = System.currentTimeMillis();
//        InsertionSort(array100);
//        System.out.println("InsertionSort array100: " + (System.currentTimeMillis() - time));
//
//        time = System.currentTimeMillis();
//        InsertionSort(array1000);
//        System.out.println("InsertionSort array1000: " + (System.currentTimeMillis() - time));
//
//        time = System.currentTimeMillis();
//        InsertionSort(array10000);
//        System.out.println("InsertionSort array10000: " + (System.currentTimeMillis() - time));
//
//        time = System.currentTimeMillis();
//        InsertionSort(array100000);
//        System.out.println("InsertionSort array100000: " + (System.currentTimeMillis() - time));
//
//        time = System.currentTimeMillis();
//        InsertionSort(arrayR100);
//        System.out.println("InsertionSort arrayR100: " + (System.currentTimeMillis() - time));
//
//        time = System.currentTimeMillis();
//        InsertionSort(arrayR1000);
//        System.out.println("InsertionSort arrayR1000: " + (System.currentTimeMillis() - time));
//
//        time = System.currentTimeMillis();
//        InsertionSort(arrayR10000);
//        System.out.println("InsertionSort arrayR10000: " + (System.currentTimeMillis() - time));
//
//        time = System.currentTimeMillis();
//        InsertionSort(arrayR100000);
//        System.out.println("InsertionSort arrayR100000: " + (System.currentTimeMillis() - time));
//
//        time = System.currentTimeMillis();
//        InsertionSort(array10P100);
//        System.out.println("InsertionSort array10P100: " + (System.currentTimeMillis() - time));
//
//        time = System.currentTimeMillis();
//        InsertionSort(array10P1000);
//        System.out.println("InsertionSort array10P1000: " + (System.currentTimeMillis() - time));
//
//        time = System.currentTimeMillis();
//        InsertionSort(array10P10000);
//        System.out.println("InsertionSort array10P10000: " + (System.currentTimeMillis() - time));
//
//        time = System.currentTimeMillis();
//        InsertionSort(array10P100000);
//        System.out.println("InsertionSort array10P100000: " + (System.currentTimeMillis() - time));
//
//        time = System.currentTimeMillis();
//        InsertionSort(array5E100);
//        System.out.println("InsertionSort array5E100: " + (System.currentTimeMillis() - time));
//
//        time = System.currentTimeMillis();
//        InsertionSort(array5E1000);
//        System.out.println("InsertionSort array5E1000: " + (System.currentTimeMillis() - time));
//
//        time = System.currentTimeMillis();
//        InsertionSort(array5E10000);
//        System.out.println("InsertionSort array5E10000: " + (System.currentTimeMillis() - time));
//
//        time = System.currentTimeMillis();
//        InsertionSort(array5E100000);
//        System.out.println("InsertionSort array5E100000: " + (System.currentTimeMillis() - time));

        ShellSortGap4(array100);
        System.out.println("ShellSortGap4 array100: " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        ShellSortGap4(array1000);
        System.out.println("ShellSortGap4 array1000: " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        ShellSortGap4(array10000);
        System.out.println("ShellSortGap4 array10000: " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        ShellSortGap4(array100000);
        System.out.println("ShellSortGap4 array100000: " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        ShellSortGap4(arrayR100);
        System.out.println("ShellSortGap4 arrayR100: " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        ShellSortGap4(arrayR1000);
        System.out.println("ShellSortGap4 arrayR1000: " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        ShellSortGap4(arrayR10000);
        System.out.println("ShellSortGap4 arrayR10000: " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        ShellSortGap4(arrayR100000);
        System.out.println("ShellSortGap4 arrayR100000: " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        ShellSortGap4(array10P100);
        System.out.println("ShellSortGap4 array10P100: " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        ShellSortGap4(array10P1000);
        System.out.println("ShellSortGap4 array10P1000: " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        ShellSortGap4(array10P10000);
        System.out.println("ShellSortGap4 array10P10000: " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        ShellSortGap4(array10P100000);
        System.out.println("ShellSortGap4 array10P100000: " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        ShellSortGap4(array5E100);
        System.out.println("ShellSortGap4 array5E100: " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        ShellSortGap4(array5E1000);
        System.out.println("ShellSortGap4 array5E1000: " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        ShellSortGap4(array5E10000);
        System.out.println("ShellSortGap4 array5E10000: " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        ShellSortGap4(array5E100000);
        System.out.println("ShellSortGap4 array5E100000: " + (System.currentTimeMillis() - time));
    }

    private static long[] make10percentArray(long[] dest, long[] source) {
        var result = Arrays.copyOf(dest, dest.length);
        var c = dest.length / 10;
        var t = c / 10;
        for(int i = 0; i < dest.length; i += c) {
            for(int j = i; j < i + t; j++) {
                result[j] = source[j];
            }
        }

        return result;
    }

    private static long[] make5elementsArray(long[] dest, long[] source) {
        var result = Arrays.copyOf(dest, dest.length);

        int i1 = 0;
        int i2 = result.length / 5;
        int i3 = i2 * 2;
        int i4 = i2 * 3;
        int i5 = result.length - 1;

        result[i1] = source[i1];
        result[i2] = source[i2];
        result[i3] = source[i3];
        result[i4] = source[i4];
        result[i5] = source[i5];

        return result;
    }
}
