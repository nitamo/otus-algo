import model.*;

import java.util.Date;

public class Program {

    public static void main(String[] args) {
        IArray array = new VectorArray(1000);
        //testAddArray(array, 10);
        //testAddArray(array, 100);
        //testAddArray(array, 1000);
        //testAddArray(array, 10_000);
        //testAddArray(array, 100_000);
        //testAddArray(array, 1_000_000);
        testAddArray(array, 10_000_000);
        //testAddArray(array, 27141398 - 5);
        testInsertArray(array);
        testRemoveArray(array);
    }

    private static void testAddArray(IArray data, int total) {
        long start = System.currentTimeMillis();

        for (int j = 0; j < total; j ++)
            data.add(new Date());

        System.out.println(data + " testAddArray: " +
                (System.currentTimeMillis() - start));
    }

    private static void testInsertArray(IArray data) {
        long start = System.currentTimeMillis();

        int size = data.size();
        int p33 = size / 3;
        int p50 = size / 2;
        int p66 = p33 * 2;
        data.add(new Date(), 0);
        data.add(new Date(), p33);
        data.add(new Date(), p50);
        data.add(new Date(), p66);
        data.add(new Date(), data.size());

        System.out.println(data + " testInsertArray: " +
                (System.currentTimeMillis() - start));
    }

    private static void testRemoveArray(IArray data) {
        long start = System.currentTimeMillis();

        int size = data.size();
        int p33 = size / 3;
        int p50 = size /2;
        int p66 = p33 * 2;
        data.remove( 0);
        data.remove(p33);
        data.remove(p50);
        data.remove(p66);
        data.remove(data.size() - 1);

        System.out.println(data + " testRemoveArray: " +
                (System.currentTimeMillis() - start));
    }

    private static void printArray(IArray data) {
        int size = data.size();
        for(int i = 0; i < size; i++) {
            System.out.printf("[%d]", data.get(i));
            if (i < size - 1)
                System.out.print(',');
            else
                System.out.print('\n');
        }

    }
}
