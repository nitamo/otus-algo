package model;

import java.util.Arrays;

public class SparseArray<T> implements ISparseArray<T>{
    private static final int factor = 50;

    int[] keys;
    Object[] values;
    int size;

    public SparseArray(int initial_size) {
        keys = new int[initial_size];
        Arrays.fill(keys, Integer.MAX_VALUE);
        values = new Object[initial_size];
        size = 0;
    }

    public SparseArray() {
        this(10);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return keys.length;
    }

    @Override
    public void add(int key, T value) {
        if(size == capacity())
            resize();
        int index = Arrays.binarySearch(keys, key);
        if(index < 0) {
            index *= -1;
            index--;
            if(index == capacity()) {
                index = size;
            }
            else {
                if(values[index] != null) {
                    move_elements_right(index);
                }
            }
            size++;
        }
        keys[index] = key;
        values[index] = value;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T get(int key) {
        int index = Arrays.binarySearch(keys, key);
        if(index < 0) {
            return null;
        }
        return (T)values[index];
    }

    @Override
    public int getKeyAt(int index) {
        return keys[index];
    }

    @Override
    @SuppressWarnings("unchecked")
    public T getValueAt(int index) {
        return (T)values[index];
    }

    @Override
    public void set(int key, T value) {
        int index = Arrays.binarySearch(keys, key);
        if(index >= 0) {
            values[index] = value;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public T remove(int key) {
        int index = Arrays.binarySearch(keys, key);
        if(index < 0) {
            return null;
        }

        Object result = values[index];
        move_elements_left(index);
        size--;
        keys[size] = Integer.MAX_VALUE;
        values[size] = null;
        return (T)result;
    }

    private void move_elements_left(int index) {
        System.arraycopy(keys, index + 1, keys, index, size - index - 1);
        System.arraycopy(values, index + 1, values, index, size - index - 1);
    }

    private void move_elements_right(int index) {
        System.arraycopy(keys, index, keys, index + 1, size - index);
        System.arraycopy(values, index, values, index + 1, size - index);
    }

    private void resize() {
        int[] newArrayKeys = new int[keys.length + keys.length * factor / 100];
        Arrays.fill(newArrayKeys, Integer.MAX_VALUE);
        System.arraycopy(keys, 0, newArrayKeys, 0, keys.length);
        keys = newArrayKeys;

        Object[] newArrayValues = new Object[values.length + values.length * factor / 100];
        System.arraycopy(values, 0, newArrayValues, 0, values.length);
        values = newArrayValues;
    }
}
