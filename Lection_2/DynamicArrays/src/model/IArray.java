package model;

public interface IArray<T> {
    int size();
    int capacity();
    void add(T item);
    T get(int index);
    void set(T item, int index);
    void add(T item, int index); // with shift to tail
    T remove(int index); // return deleted element
}
