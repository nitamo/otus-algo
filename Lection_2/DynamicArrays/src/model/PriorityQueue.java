package model;

import java.util.Arrays;

public class PriorityQueue<T> implements IPriorityQueue<T>{
    private static final int factor = 50;

    int[] keys;
    Object[] values;
    int size;

    public PriorityQueue(int initial_size) {
        keys = new int[initial_size];
        Arrays.fill(keys, Integer.MAX_VALUE);
        values = new Object[initial_size];
        size = 0;
    }

    public PriorityQueue() {
        this(10);
    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return keys.length;
    }

    @Override
    public void push(int priority, T value) {
        if(size == capacity())
            resize();
        int index = Arrays.binarySearch(keys, priority);
        if(index < 0) {
            index *= -1;
            index--;
            if(index == capacity()) {
                index = size;
            }
        }
        move_elements_right(index);
        keys[index] = priority;
        values[index] = value;
        size++;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T pop() {
        if(size == 0) {
            return null;
        }
        size--;
        Object result = values[size];
        keys[size] = Integer.MAX_VALUE;
        values[size] = null;

        return (T) result;
    }

    private void move_elements_right(int index) {
        System.arraycopy(keys, index, keys, index + 1, size - index);
        System.arraycopy(values, index, values, index + 1, size - index);
    }

    private void resize() {
        int[] newArrayKeys = new int[keys.length + keys.length * factor / 100];
        Arrays.fill(newArrayKeys, Integer.MAX_VALUE);
        System.arraycopy(keys, 0, newArrayKeys, 0, keys.length);
        keys = newArrayKeys;

        Object[] newArrayValues = new Object[values.length + values.length * factor / 100];
        System.arraycopy(values, 0, newArrayValues, 0, values.length);
        values = newArrayValues;
    }
}
