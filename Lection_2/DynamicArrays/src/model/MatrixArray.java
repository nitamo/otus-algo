package model;

public class MatrixArray<T> implements IArray<T> {

    private int size;
    private int vector;
    private IArray<IArray<T>> array;

    public MatrixArray(int vector) {
        this.vector = vector;
        array = new SingleArray<>();
        size = 0;
    }

    public MatrixArray() {
        this(10);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return size;
    }

    @Override
    public void add(T item) {
        add(item, size);
    }

    @Override
    public T get(int index) {
        return array.get(index / vector).get(index % vector);
    }

    @Override
    public void set(T item, int index) {
        array.get(index / vector).set(item, index % vector);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void add(T item, int index) {
        if (size == array.size() * vector)
            array.add(new VectorArray<>(vector));
        for(int i = size - 1; i >= index; i--)
        {
            T current_item = array.get(i / vector).get(i % vector);
            int next_index = i + 1;
            array.get(next_index / vector).set(current_item, next_index % vector);
        }
        array.get(index / vector).set(item, index % vector);
        size++;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T remove(int index) {
        T result = array.get(index / vector).get(index % vector);

        for(int i = index; i < (size - 1); i++)
        {
            int next_index = i + 1;
            T current_item = array.get(next_index / vector).get(next_index % vector);
            array.get(i / vector).set(current_item, i % vector);
        }
        size--;
        array.get(size / vector).set(null, size % vector);

        return result;
    }
}
