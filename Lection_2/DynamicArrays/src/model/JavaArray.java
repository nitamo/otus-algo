package model;

import java.util.ArrayList;
import java.util.List;

public class JavaArray<T> implements IArray<T> {
    List<T> array;

    public JavaArray() {
        array = new ArrayList<>();
    }

    @Override
    public int size() {
        return array.size();
    }

    @Override
    public int capacity() {
        return array.size();
    }

    @Override
    public void add(T item) {
        array.add(item);
    }

    @Override
    public T get(int index) {
        return array.get(index);
    }

    @Override
    public void set(T item, int index) {
        array.set(index, item);
    }

    @Override
    public void add(T item, int index) {
        array.add(index, item);
    }

    @Override
    public T remove(int index) {
        return array.remove(index);
    }
}
