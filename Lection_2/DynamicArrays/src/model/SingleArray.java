package model;

public class SingleArray<T> implements IArray<T> {

    private Object[] array;
    int size;

    public SingleArray () {
        array = new Object[0];
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return array.length;
    }

    @Override
    public void add(T item) {
        add(item, size);
    }

    @Override
    @SuppressWarnings("unchecked")
    public T get(int index) {
        return (T)array[index];
    }

    @Override
    public void set(T item, int index) {
        array[index] = item;
    }

    @Override
    public void add(T item, int index) {
        resize(index);
        size++;
        array[index] = item;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T remove(int index) {
        Object result = array[index];

        System.arraycopy(array, index + 1, array, index, size - index - 1);
        size--;
        array[size] = null;

        return (T)result;
    }

    private void resize(int index) {
        Object[] newArray = new Object[capacity() + 1];
        System.arraycopy(array, 0, newArray, 0, index);
        System.arraycopy(array, index, newArray, index + 1, size - index);
        array = newArray;
    }
}
