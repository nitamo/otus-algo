package model;

public class FactorArray<T> implements IArray<T> {

    private Object[] array;
    private int factor;
    private int size;

    public FactorArray(int factor, int initLength) {
        this.factor = factor;
        initLength = (initLength > 1) ? initLength : 2;
        array = new Object[initLength];
        size = 0;
    }

    public FactorArray() {
        this(50, 10);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return array.length;
    }

    @Override
    public void add(T item) {
        add(item, size);
    }

    @Override
    @SuppressWarnings("unchecked")
    public T get(int index) {
        return (T)array[index];
    }

    @Override
    public void set(T item, int index) {
        array[index] = item;
    }

    @Override
    public void add(T item, int index) {
        if(size() == capacity()) {
            resize();
        }

        if(index < size) {
            System.arraycopy(array, index, array, index + 1, size - index);
        }

        size++;
        array[index] = item;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T remove(int index) {
        Object result = array[index];

        System.arraycopy(array, index + 1, array, index, size - index - 1);
        size--;
        array[size] = null;

        return (T)result;
    }

    private void resize() {
        Object[] newArray = new Object[array.length + array.length * factor / 100];
        System.arraycopy(array, 0, newArray, 0, array.length);
        array = newArray;
    }
}
