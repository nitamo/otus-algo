package model;

public class VectorArray<T> implements IArray<T> {

    private Object[] array;
    private int vector;
    private int size;

    public VectorArray(int vector) {
        this.vector = vector;
        array = new Object[vector];
        size = 0;
    }

    public VectorArray() {
        this(1);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return array.length;
    }

    @Override
    public void add(T item) {
        add(item, size);
    }

    @Override
    @SuppressWarnings("unchecked")
    public T get(int index) {
        return (T)array[index];
    }

    @Override
    public void set(T item, int index) {
        array[index] = item;
    }

    @Override
    public void add(T item, int index) {
        if(size() == capacity()) {
            resize();
        }

        if(index < size) {
            System.arraycopy(array, index, array, index + 1, size - index);
        }

        size++;
        array[index] = item;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T remove(int index) {
        Object result = array[index];

        System.arraycopy(array, index + 1, array, index, size - index - 1);
        size--;
        array[size] = null;

        return (T)result;
    }

    private void resize() {
        Object[] newArray = new Object[array.length + vector];
        System.arraycopy(array, 0, newArray, 0, array.length);
        array = newArray;
    }
}
