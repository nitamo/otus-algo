package model;

public interface IPriorityQueue<T> {
    int size();
    int capacity();
    void push(int priority, T value);
    T pop();
}
