package model;

public interface ISparseArray<T> {
    int size();
    int capacity();
    void add(int key, T value);
    T get(int key);
    int getKeyAt(int index);
    T getValueAt(int index);
    void set(int key, T value);
    T remove(int key);
}
