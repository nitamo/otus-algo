public class Main {

    public static void main(String[] args) {
        //HashTable<Integer, Integer> table = new BSTHashTable<>(3);
        var table = new OpenAddressHashTable<>();
        table.addValue(1, 1);
        table.addValue(10, 10);
        table.addValue(100, 100);
        table.addValue(101, 101);
        table.addValue(102, 102);
        table.addValue(103, 103);
        table.addValue(104, 104);
        table.addValue(105, 105);
    }
}
