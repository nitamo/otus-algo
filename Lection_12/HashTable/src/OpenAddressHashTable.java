public class OpenAddressHashTable<K, V> implements HashTable<K, V> {

    private static final int INITIAL_CAPACITY = 13;

    private Pair<K, V>[] data;
    private int size;
    private int capacity;

    public OpenAddressHashTable() {
        capacity = INITIAL_CAPACITY;
        data = new Pair[capacity];
    }

    public OpenAddressHashTable(int initialCapacity) {
        capacity = findNextPrime(initialCapacity);
        data = new Pair[capacity];
    }

    @Override
    public V getValue(K key) {
        var index = key.hashCode() % capacity;
        Pair<K, V> cell;
        for(int i = 1; i < size + 1 && index < Integer.MAX_VALUE; i++, index += i * i) {
            cell = data[index % capacity];
            if(cell != null && cell.key.equals(key)) {
                return cell.value;
            }
        }
        return null;
    }

    @Override
    public V addValue(K key, V value) {
        if(capacity / (size + 1) < 2) {
            resize();
        }

        var index = key.hashCode() % capacity;
        Pair<K, V> cell;
        for(int i = 1; index < Integer.MAX_VALUE; i++, index += i * i) {
            cell = data[index % capacity];
            if(cell != null) {
                if (cell.key.equals(key)) {
                    var result = cell.value;
                    cell.value = value;
                    return result;
                }
            }
            else {
                data[index % capacity] = new Pair<>(key, value);
                size++;
                return value;
            }
        }
        return null;
    }

    @Override
    public V deleteValue(K key) {
        var index = key.hashCode() % capacity;
        Pair<K, V> cell;
        for(int i = 1; i < size + 1 && index < Integer.MAX_VALUE; i++, index += i * i) {
            cell = data[index % capacity];
            if(cell != null) {
                if (cell.key.equals(key)) {
                    var result = cell.value;
                    data[index % capacity] = null;
                    size--;
                    return result;
                }
            }
        }
        return null;
    }

    @Override
    public V setValue(K key, V value) {
        var index = key.hashCode() % capacity;
        Pair<K, V> cell;
        for(int i = 1, j = 0; j < size && index < Integer.MAX_VALUE; i++, index += i * i, j++) {
            cell = data[index % capacity];
            if(cell != null) {
                if (cell.key.equals(key)) {
                    var result = cell.value;
                    cell.value = value;
                    return result;
                }
            }
        }
        return null;
    }

    private void resize() {
        int newCapacity = findNextPrime(capacity * 2);
        var newData = new Pair[newCapacity];

        for (var pair: data) {
            if(pair != null) {
                var index = pair.key.hashCode() % newCapacity;
                for(int i = 1; index < Integer.MAX_VALUE; i++, index += i * i) {
                    if(newData[index % newCapacity] == null) {
                        newData[index % newCapacity] = pair;
                        break;
                    }
                }
            }
        }

        capacity = newCapacity;
        data = newData;
    }

    private static int findNextPrime(int limit) {
        var result = limit + 1;
        while(true) {
            if(isPrime(result)) {
                return result;
            }
            result++;
        }
    }

    private static boolean isPrime(int value) {
        for(int i = 2; i * i <= value; i++) {
            if(value % i == 0) {
                return false;
            }
        }
        return true;
    }

}
