import java.util.LinkedList;

public class BSTHashTable<K, V> implements HashTable<K, V>{

    private static final int INITIAL_CAPACITY = 10;

    private Cell<K, V> data[];

    private int capacity;
    private int size;

    @SuppressWarnings("Unckecked")
    public BSTHashTable(int initialCapacity) {
        data = new Cell[initialCapacity];
        capacity = initialCapacity;
    }

    public BSTHashTable() {
        this(INITIAL_CAPACITY);
    }

    @Override
    public V getValue(K key) {
        int index = key.hashCode() % capacity;
        if(data[index] == null) {
            return null;
        }
        var result = data[index].getValue(key);
        return result;
    }

    @Override
    public V addValue(K key, V value) {
        if(capacity / (size + 1) < 2) {
            resize();
        }
        int index = key.hashCode() % capacity;
        if(data[index] == null) {
            data[index] = new Cell<>();
        }
        var result = data[index].addValue(key, value);
        if(result) {
            size++;
        }
        return value;
    }

    @Override
    public V deleteValue(K key) {
        int index = key.hashCode() % capacity;
        if(data[index] == null) {
            return null;
        }
        var result = data[index].deleteValue(key);
        if(result != null) {
            size--;
        }
        return result;
    }

    @Override
    public V setValue(K key, V value) {
        int index = key.hashCode() % capacity;
        if(data[index] == null) {
            return null;
        }
        return data[index].setValue(key, value);
    }

    @SuppressWarnings("Unckecked")
    private void resize() {
        int newCapacity = capacity * 2;
        var newData = new Cell[newCapacity];
        for (var cell: data) {
            if(cell != null && cell.data != null) {
                for (var pair : cell.data) {
                    int index = pair.key.hashCode() % newCapacity;
                    if(newData[index] == null) {
                        newData[index] = new Cell<>();
                    }
                    newData[index].addValue(pair.key, pair.value);
                }
            }
        }
        capacity = newCapacity;
        data = newData;
    }
}

class Cell<K, V> {
    LinkedList<Pair<K, V>> data = new LinkedList<>();

    public Cell() {

    }

    public V getValue(K key) {
        var element = findElement(key);
        return element == null ? null : element.value;
    }

    public V setValue(K key, V value) {
        var element = findElement(key);
        if(element == null) {
            return null;
        }
        else {
            return setValueInternal(element, value);
        }
    }

    public boolean addValue(K key, V value) {
        var element = findElement(key);
        if(element != null) {
            setValueInternal(element, value);
            return false;
        }
        else {
            data.add(new Pair<>(key, value));
            return true;
        }

    }

    public V deleteValue(K key) {
        var element = findElement(key);
        if(element == null) {
            return null;
        }
        else {
            var result = element.value;
            data.remove(element);
            return result;
        }
    }

    private Pair<K, V> findElement(K key) {
        Pair<K, V> result = null;
        for (var element: data) {
            if(element.key.equals(key)) {
                result = element;
            }
        }
        return result;
    }

    private V setValueInternal(Pair<K, V> element, V value) {
        var result = element.value;
        element.value = value;
        return result;
    }
}
