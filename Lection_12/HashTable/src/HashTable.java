public interface HashTable<K, V> {
    V getValue(K key);
    V addValue(K key, V value);
    V deleteValue(K key);
    V setValue(K key, V value);
}
