/*
* Xor encryption/decryption utility.
* Usage: app [-e/d] [file_name] [key_file_name]
* The key file is a text file which contain a 32-bit integer.
* The key will be truncated/resized to 4 bytes.
*/

package com.nitamo;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;

public class Main {

    public static void main(String[] args) {
        if(args.length != 3) {
            System.out.println("Usage: app [-e/d] [file_name] [key_file_name]");
            return;
        }

        long key = 0;
        try (var keyReader = new BufferedReader(new FileReader(args[2]))) {
            var keyString = keyReader.readLine();
            key = Long.parseUnsignedLong(keyString, 0x10);
        }
        catch (Exception ex) {
            System.out.println("Cannot read key file.");
        }

        boolean decrypt = false;
        try {
            decrypt = args[0].charAt(args[0].length() - 1) == 'd';
        }
        catch (Exception ex) {
            System.out.println("Invalid operation argument.");
            System.out.println("Usage: app [-e/d] [file_name] [key_file_name]");
        }

        var keyBytes = new byte[4];
        for (int i = 0; i < 4; i++) {
            var shift = i * 8;
            var mask = 0xFF << shift;
            keyBytes[i] = (byte)((key & mask) >> shift);
        }

        var inputBuffer = new byte[4];
        var prevEncBytes = new byte[4];
        byte temp;
        boolean chaining = false;

        try (var output = new FileOutputStream("output")) {
            try (var input = new FileInputStream(args[1])) {
                for (var len = input.read(inputBuffer); len > 0; len = input.read(inputBuffer), chaining = true) {
                    for (int i = 0; i < len; i++) {
                        if(decrypt) {
                            temp = (byte) (inputBuffer[i] ^ keyBytes[i]);
                            if(chaining) {
                                temp = (byte)(temp ^ prevEncBytes[i]);
                            }
                            prevEncBytes[i] = inputBuffer[i];
                        }
                        else {
                            if(chaining) {
                                temp = (byte) (inputBuffer[i] ^ prevEncBytes[i]);
                            }
                            else {
                                temp = inputBuffer[i];
                            }
                            temp = (byte) (temp ^ keyBytes[i]);
                            prevEncBytes[i] = temp;
                        }
                        output.write(temp);
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("Cannot read input file.");
            }
        }
        catch (Exception ex) {
            System.out.println("Cannot write output file.");
        }
    }
}
