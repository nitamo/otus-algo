﻿using System;
using System.Collections.Generic;

namespace HorseMove
{
    public class FenBoard
    {
        Cell[] board = new Cell[64];
        Color nextMove = Color.White;
        List<Castling> castlings = new List<Castling>();
        Position enPassant = new Position();
        int halfmoves;
        int fullmoves;

        public FenBoard() : this("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
        {
        }

        public FenBoard(String fen)
        {
            var fenItems = fen.Split(' ');
            var fenBoard = fenItems[0];
            CreateBoard(fenBoard);
            WhoseMove(fenItems[1]);
            CreateCastlings(fenItems[2]);
            CreateEnPassant(fenItems[3]);
            halfmoves = int.Parse(fenItems[4]);
            fullmoves = int.Parse(fenItems[5]);
        }

        private void CreateBoard(String fenBoard)
        {
            var rows = fenBoard.Split('/');

            for(int i = 0; i < 8; i++)
            {
                for(int j = 0, k = 0; j < rows[i].Length; j++)
                {
                    string str = rows[i][j].ToString();

                    if (int.TryParse(str, out int value))
                    {
                        int limit = k + value;
                        for (; k < limit; k++)
                        {
                            board[8 * i + k] = new Cell();
                        }
                    }
                    else
                    {
                        PlaceFigure(i, k, str);
                        k++;
                    }

                }
            }
        }

        private void PlaceFigure(int row, int column, string str)
        {
            switch (str)
            {
                case "p":
                    board[8 * row + column] = new Pawn(Color.Black);
                    break;
                case "P":
                    board[8 * row + column] = new Pawn(Color.White);
                    break;
                case "n":
                    board[8 * row + column] = new Knight(Color.Black);
                    break;
                case "N":
                    board[8 * row + column] = new Knight(Color.White);
                    break;
                case "b":
                    board[8 * row + column] = new Bishop(Color.Black);
                    break;
                case "B":
                    board[8 * row + column] = new Bishop(Color.White);
                    break;
                case "r":
                    board[8 * row + column] = new Rook(Color.Black);
                    break;
                case "R":
                    board[8 * row + column] = new Rook(Color.White);
                    break;
                case "q":
                    board[8 * row + column] = new Queen(Color.Black);
                    break;
                case "Q":
                    board[8 * row + column] = new Queen(Color.White);
                    break;
                case "k":
                    board[8 * row + column] = new King(Color.Black);
                    break;
                case "K":
                    board[8 * row + column] = new King(Color.White);
                    break;
            }
        }

        private void WhoseMove(string str)
        {
            nextMove = str == "w" ? Color.White : Color.Black;
        }

        private void CreateCastlings(string str)
        {
            if(str == "-")
            {
                return;
            }

            for(int i = 0; i < str.Length; i++)
            {
                switch (str[i])
                {
                    case 'K':
                        castlings.Add(Castling.WhiteKingside);
                        break;
                    case 'Q':
                        castlings.Add(Castling.WhiteQueenside);
                        break;
                    case 'k':
                        castlings.Add(Castling.BlackKingside);
                        break;
                    case 'q':
                        castlings.Add(Castling.BlackQueenside);
                        break;
                }
            }
        }

        private void CreateEnPassant(string str)
        {
            if(str == "-")
            {
                return;
            }
            enPassant = new Position(str);
        }

        public string ToFenString()
        {
            string result = "";

            for(int i = 0; i < 8; i++)
            {
                int k = 0;
                for(int j = 0; j < 8; j++)
                {
                    var item = board[i * 8 + j];
                    if (item is Figure)
                    {
                        if(k != 0)
                        {
                            result += k;
                            k = 0;
                        }
                        result += item.ToString();
                    }
                    else
                    {
                        k++;
                    }
                }
                if(k != 0)
                {
                    result += k;
                }

                result += (i + 1 < 8) ? "/" : "";
            }

            result += " ";
            result += (nextMove == Color.White) ? "w" : "b";

            result += " ";

            if(castlings.Count > 0)
            {
                for(int i = 0; i < castlings.Count; i++)
                {
                    switch(castlings[i])
                    {
                        case Castling.WhiteKingside:
                            result += "K";
                            break;
                        case Castling.WhiteQueenside:
                            result += "Q";
                            break;
                        case Castling.BlackKingside:
                            result += "k";
                            break;
                        case Castling.BlackQueenside:
                            result += "q";
                            break;
                    }
                }
            }
            else
            {
                result += "-";
            }

            result += " ";

            if(enPassant.IsEmpty())
            {
                result += "-";
            }
            else
            {
                result += enPassant.ToString();
            }

            result += " ";

            result += halfmoves;

            result += " ";

            result += fullmoves;

            return result;
        }

        public override string ToString()
        {
            string result = "";
            result += "   + - - - - - - - - +\n";

            for(int i = 0, j = 8; i < 8; i++, j--)
            {
                result += " " + j + " | ";

                for(int k = 0; k < 8; k++)
                {
                    result += board[i * 8 + k].ToString() + " ";
                }

                result += "|\n";
            }

            result += "   + - - - - - - - - +\n";
            result += "     a b c d e f g h";
            return result;
        }

        public void MakeMove(string move)
        {
            var sPos = new Position(move.Substring(0, 2));
            var ePos = new Position(move.Substring(2, 2));

            nextMove = (nextMove == Color.White) ? Color.Black : Color.White;

            if(nextMove == Color.White)
            {
                fullmoves++;
            }

            var figure = board[sPos.ToCellNumber()];
            var target = board[ePos.ToCellNumber()];

            if (target.IsEmpty() && !(figure is Pawn))
            {
                halfmoves++;
            }

            if(figure is Pawn)
            {
                if(ePos.Row == 1 || ePos.Row == 8)
                {
                    var newFigure = move.Substring(4, 1);
                    PlaceFigure(ePos.RowOrdinal, ePos.Column, newFigure);
                    board[sPos.ToCellNumber()] = new Cell();

                    return;
                }

                if(ePos.Equals(enPassant))
                {
                    if(ePos.Row == 3)
                    {
                        board[ePos.ToCellNumber() - 8] = new Cell();
                    }
                    else
                    {
                        board[ePos.ToCellNumber() + 8] = new Cell();
                    }

                    board[ePos.ToCellNumber()] = figure;
                    board[sPos.ToCellNumber()] = new Cell();
                    enPassant = new Position();

                    return;
                }

                if(ePos.Row - sPos.Row > 1 || sPos.Row - ePos.Row > 1)
                {
                    bool nearPawn = false;
                    if(ePos.Column > 0)
                    {
                        var cell = board[ePos.ToCellNumber() - 1];
                        nearPawn = !cell.IsEmpty() && 
                            cell is Pawn && 
                            !(cell.ToString().Equals(figure.ToString()));
                    }
                    if(ePos.Column < 7 && !nearPawn)
                    {
                        var cell = board[ePos.ToCellNumber() + 1];
                        nearPawn = !cell.IsEmpty() &&
                            cell is Pawn &&
                            !(cell.ToString().Equals(figure.ToString()));
                    }
                    if(nearPawn)
                    {
                        enPassant = new Position((ePos.Row + sPos.Row) / 2, ePos.Column);
                    }

                    board[ePos.ToCellNumber()] = figure;
                    board[sPos.ToCellNumber()] = new Cell();

                    return;
                }
            }

            if(figure is Rook)
            {
                var rook = figure as Rook;
                if(sPos.Column == 0 && rook.Player == Color.Black)
                {
                    castlings.Remove(Castling.BlackQueenside);
                }
                else if(sPos.Column == 7 && rook.Player == Color.Black)
                {
                    castlings.Remove(Castling.BlackKingside);
                }
                else if (sPos.Column == 0 && rook.Player == Color.White)
                {
                    castlings.Remove(Castling.WhiteQueenside);
                }
                else if (sPos.Column == 7 && rook.Player == Color.White)
                {
                    castlings.Remove(Castling.WhiteKingside);
                }
            }

            if(figure is King)
            {
                var king = figure as King;
                if(ePos.Column - sPos.Column > 1 || sPos.Column - ePos.Column > 1)
                {
                    if(king.Player == Color.White)
                    {
                        if(ePos.Column > sPos.Column)
                        {
                            castlings.Remove(Castling.WhiteKingside);
                            PlaceFigure(ePos.RowOrdinal, ePos.Column - 1, "R");
                            board[63] = new Cell();
                        }
                        else
                        {
                            castlings.Remove(Castling.WhiteQueenside);
                            PlaceFigure(ePos.RowOrdinal, ePos.Column + 1, "R");
                            board[56] = new Cell();
                        }
                    }
                    else
                    {
                        if (ePos.Column > sPos.Column)
                        {
                            castlings.Remove(Castling.BlackKingside);
                            PlaceFigure(ePos.RowOrdinal, ePos.Column - 1, "r");
                            board[7] = new Cell();
                        }
                        else
                        {
                            castlings.Remove(Castling.BlackQueenside);
                            PlaceFigure(ePos.RowOrdinal, ePos.Column + 1, "r");
                            board[0] = new Cell();
                        }
                    }
                }
                else
                {
                    if(castlings.Count != 0)
                    {
                        if(king.Player == Color.White)
                        {
                            castlings.Remove(Castling.WhiteKingside);
                            castlings.Remove(Castling.WhiteQueenside);
                        }
                        else
                        {
                            castlings.Remove(Castling.BlackKingside);
                            castlings.Remove(Castling.BlackQueenside);
                        }
                    }
                }

                if (!enPassant.IsEmpty())
                {
                    enPassant = new Position();
                }

                return;
            }

            board[ePos.ToCellNumber()] = figure;
            board[sPos.ToCellNumber()] = new Cell();
            if(!enPassant.IsEmpty())
            {
                enPassant = new Position();
            }
        }
    }

    class Cell
    {
        public override string ToString()
        {
            return ".";
        }

        public virtual bool IsEmpty()
        {
            return true;
        }
    }

    abstract class Figure : Cell
    {
        protected Color player;

        public Color Player { get => player; }

        public override bool IsEmpty()
        {
            return false;
        }
    }

    enum Color    
    {
        White,
        Black,
    }

    class Pawn : Figure 
    {
        public Pawn(Color color) { player = color; }

        public override string ToString()
        {
            return player == Color.Black ? "p" : "P";
        }
    }

    class Knight : Figure
    {
        public Knight(Color color) { player = color; }

        public override string ToString()
        {
            return player == Color.Black ? "n" : "N";
        }
    }

    class Bishop : Figure
    {
        public Bishop(Color color) { player = color; }

        public override string ToString()
        {
            return player == Color.Black ? "b" : "B";
        }
    }

    class Rook : Figure
    {
        public Rook(Color color) { player = color; }

        public override string ToString()
        {
            return player == Color.Black ? "r" : "R";
        }
    }

    class Queen : Figure
    {
        public Queen(Color color) { player = color; }

        public override string ToString()
        {
            return player == Color.Black ? "q" : "Q";
        }
    }

    class King : Figure
    {
        public King(Color color) { player = color; }

        public override string ToString()
        {
            return player == Color.Black ? "k" : "K";
        }
    }

    enum Castling
    {
        WhiteKingside,
        WhiteQueenside,
        BlackKingside,
        BlackQueenside
    }

    class Position
    {
        int column;
        int row;

        public int Column { get => column; }
        public int Row { get => row; }
        public int RowOrdinal { get => 8 - row; }

        public Position()
        {
            column = row = -1;
        }

        public Position(string str)
        {
            column = str[0] - 'a';
            row = int.Parse(str[1].ToString());
        }

        public Position(int cell)
        {
            if(cell < 0 || cell > 63)
            {
                column = row = -1;
            }

            row = 8 - cell / 8;
            column = cell % 8;
        }

        public Position(int row, int column)
        {
            this.row = row;
            this.column = column;
        }

        public bool IsEmpty()
        {
            if(column == -1 || row == -1)
            {
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            string result = "";
            result += (char)('a' + column);
            result += row;
            return result;
        }

        public int ToCellNumber()
        {
            int result = (8 - row) * 8;
            result += column;
            return result;
        }

        public override bool Equals(object obj)
        {
            if(obj == null || !(obj is Position))
            {
                return false;
            }

            var other = obj as Position;
            if(other.column == column && other.row == row)
            {
                return true;
            }
            return false;
        }
    }
}
