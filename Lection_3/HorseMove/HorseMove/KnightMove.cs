﻿using System;
namespace KnightMove
{
    public class KnightMove
    {
        static ulong[] moveKnignt(ulong position)
        {
            var result = new ulong[2];
            ulong NoGH = 0x3f3f3f3f3f3f3f3fUL;
            ulong NoH = 0x7f7f7f7f7f7f7f7fUL;
            ulong NoAB = 0xfcfcfcfcfcfcfcfcUL;
            ulong NoA = 0xfefefefefefefefeUL;

            result[1] =
                NoGH & (position >> 10 | position << 6) |
                NoH & (position >> 17 | position << 15) |
                NoA & (position >> 15 | position << 17) |
                NoAB & (position << 10 | position >> 6);

            var bits = result[1];
            while (bits != 0)
            {
                bits &= (bits - 1);
                result[0]++;
            }

            return result;
        }

        static void fast_print_board(ulong position, ulong moves)
        {
            System.Console.WriteLine("| - - - - - - - - |");
            for (int i = 64; i >= 1; i -= 8)
            {
                System.Console.Write("| ");
                for (int j = i - 8; j < i; j++)
                {
                    if ((position >> j & 1) == 1)
                    {
                        System.Console.Write("F ");
                    }
                    else if ((moves >> j & 1) == 1)
                    {
                        System.Console.Write("0 ");
                    }
                    else
                    {
                        System.Console.Write(". ");
                    }
                }
                System.Console.WriteLine('|');
            }
            System.Console.WriteLine("| - - - - - - - - |");
        }
    }
}
