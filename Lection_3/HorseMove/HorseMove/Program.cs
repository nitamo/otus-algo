﻿using System;

namespace HorseMove
{
    class Program
    {
        static void Main(string[] args)
        {
            //var fenBoard1 = new FenBoard("rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq c6 0 2");
            var fenBoard1 = new FenBoard("rnbqkbnr/pppppppp/8/1P6/8/8/P1PPPPPP/RNBQKBNR w KQkq - 0 0");
            System.Console.WriteLine(fenBoard1.ToString());
            System.Console.WriteLine(fenBoard1.ToFenString());
            fenBoard1.MakeMove("a7a5");
            System.Console.WriteLine(fenBoard1.ToString());
            System.Console.WriteLine(fenBoard1.ToFenString());
            fenBoard1.MakeMove("b5a6");
            System.Console.WriteLine(fenBoard1.ToString());
            System.Console.WriteLine(fenBoard1.ToFenString());
            return;
        }
    }
}
