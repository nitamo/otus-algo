import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int H, W;
        int[][] field;
        int[][] heights;

        //Data input
        Scanner input = new Scanner(System.in);
        String[] inputString = input.nextLine().split(" ");
        W = Integer.parseInt(inputString[0]);
        H = Integer.parseInt(inputString[1]);
        field = new int[H][W];
        heights = new int[H][W];

        int obstaclesCount = Integer.parseInt(input.nextLine());

        for (int i = 0; i < obstaclesCount; i++) {
            String[] obstacle = input.nextLine().split(" ");
            int ox = Integer.parseInt(obstacle[0]);
            int oy = Integer.parseInt(obstacle[1]);
            field[oy][ox] = 1;
        }


        //Calculate heights
        for (int h = 0; h < H; h++) {
            for (int w = 0; w < W; w++) {
                int height = 0;
                for(int dh = h; dh >= 0; dh--) {
                    if(field[dh][w] == 1) {
                        break;
                    }
                    height++;
                }
                heights[h][w] = height;
            }
        }
        
        //Calculate areas
        int maxArea = 0;
        for (int h = 0; h < H; h++) {
            for (int w = 0; w < W; w++) {
                int currentArea = calculateArea(h, w, heights);
                maxArea = currentArea > maxArea ? currentArea : maxArea;
            }
        }
        System.out.println(maxArea);
    }

    private static int calculateArea(int h, int w, int[][] result) {
        int right = 0;
        int left = 0;
        int dw = w;
        for (; dw < result[h].length && result[h][dw] >= result[h][w]; dw++) {
            right++;
        }

        dw = w;
        for (; dw >= 0  && result[h][dw] >= result[h][w]; dw--) {
            left++;
        }
        return result[h][w] * (left + right - 1);
    }
}
