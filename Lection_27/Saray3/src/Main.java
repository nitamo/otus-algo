import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int W;
        int[] field;
        int[] left;
        int[] right;

        //Data input
        Scanner input = new Scanner(System.in);
        W = input.nextInt();
        field = new int[W];
        left = new int[W];
        right = new int[W];

        for (int i = 0; i < W; i++) {
            field[i] = input.nextInt();
        }

        for (int i = 0; i < W; i++) {
            int j = i;
            for (; j < W && field[j] >= field[i]; j++) {
            }
            right[i] = j - 1;

            j = i;
            for (; j >= 0  && field[j] >= field[i]; j--) {
            }
            left[i] = j + 1;
        }


        printA(left);
        printA(right);
    }

    private static void printA(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if(i > 0) {
                System.out.print(" ");
            }
            System.out.print(array[i]);
        }
        System.out.println();
    }
}
