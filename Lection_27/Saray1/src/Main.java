import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int H, W;
        int[][] field = null;

        //Data input
        Scanner input = new Scanner(System.in);
        String[] inputString = input.nextLine().split(" ");
        W = Integer.parseInt(inputString[0]);
        H = Integer.parseInt(inputString[1]);
        field = new int[H][W];

        for (int i = 0; i < H; i++) {
            inputString = input.nextLine().split(" ");
            for (int j = 0; j < W; j++) {
                field[i][j] = Integer.parseInt(inputString[j]);
            }
        }

        if(H <= 0 || W <= 0) {
            return;
        }

        //Calculation
        int maxSquare = 0;
        int resultSquare = 0;
        for (int h = 0; h < H; h++) {
            for (int w = 0; w < W; w++) {
                if(field[h][w] == 1) {
                    continue;
                }
                int minRowLength = findRowLength(field, w, h);
                int rectHeigth = 1;
                maxSquare = minRowLength * rectHeigth;
                for (int dh = h + 1; dh < H; dh++) {
                    if(field[dh][w] == 1) {
                        break;
                    }
                    int currRowLength = findRowLength(field, w, dh);
                    if(minRowLength > currRowLength) {
                        minRowLength = currRowLength;
                    }
                    if(currRowLength > minRowLength) {
                        currRowLength = minRowLength;
                    }

                    rectHeigth++;
                    int currSquare = rectHeigth * currRowLength;
                    if(maxSquare < currSquare) {
                        maxSquare = currSquare;
                    }
                    System.out.println("[" + h + ":" + w + "] = {" + minRowLength + ":" + rectHeigth + "} = " + rectHeigth * minRowLength);
                }
                if(resultSquare < maxSquare) {
                    resultSquare = maxSquare;
                }
            }

        }

        System.out.println(resultSquare);
    }

    private static int findRowLength(int[][] field, int x, int y) {
        int length = 0;
        for (int i = x; i < field[y].length; i++) {
            if(field[y][i] == 1) {
                break;
            }
            length++;
        }
        return length;
    }
}
