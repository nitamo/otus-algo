import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int H, W;
        int[][] field;
        //int[][] result;

        //Data input
        Scanner input = new Scanner(System.in);
        String[] inputString = input.nextLine().split(" ");
        W = Integer.parseInt(inputString[0]);
        H = Integer.parseInt(inputString[1]);
        field = new int[H][W];
        //result = new int[H][W];

        int obstaclesCount = Integer.parseInt(input.nextLine());

        for (int i = 0; i < obstaclesCount; i++) {
            String[] obstacle = input.nextLine().split(" ");
            int ox = Integer.parseInt(obstacle[0]);
            int oy = Integer.parseInt(obstacle[1]);
            field[oy][ox] = 1;
        }

        for (int i = 0; i < H; i++) {
            if(i > 0) {
                System.out.println();
            }

            for (int j = 0; j < W; j++) {
                int height = 0;
                for(int dy = i; dy >= 0; dy--) {
                    if(field[dy][j] == 1) {
                        break;
                    }
                    height++;
                }
                //result[i][j] = height;
                if(j > 0) {
                    System.out.print(" ");
                }
                System.out.print(height);
            }
        }
    }
}
